import { Module, Logger } from '@nestjs/common'
import { AuthApiKeyService } from './auth-api-key.service'
import { apiKeyStrategy } from './strategies/apiKey.strategy'
import { FofCoreModule, fofEnv } from '@fof-nestjs/core'
import { authApiKey } from './guard/api-key.guard'

@Module({
  imports: [
    FofCoreModule
  ],
  providers: [
    AuthApiKeyService,
    apiKeyStrategy
  ],
  exports: [
    AuthApiKeyService
  ],
})
export class AuthApiKeyModule {
  private readonly logger = new Logger('FofAuthApiKey') 

  constructor() {
    
  }

  async onModuleInit () {    
    this.logger.log(`Add API Key authenticaton strategy.`)
    // console.log(fofEnv())
  }

  async onApplicationShutdown() {
    //todo
  }  
}
