/** To enable intellisense capability on env file*/
export interface efofConfig {
  authApiKey: {
    header: string
    prefix: string    
  }  
}

/** 
 * Transform env vars into typed config
 * Doc here: https://docs.nestjs.com/techniques/configuration
 */
export const fofEnv = ():efofConfig => ({
  authApiKey: {
    header: process.env.AUTH_API_KEY_HEADER || 'api-key',
    prefix: process.env.AUTH_API_KEY_PREFIX || ''   
  }  
})



