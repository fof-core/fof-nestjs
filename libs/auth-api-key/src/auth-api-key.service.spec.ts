import { Test, TestingModule } from '@nestjs/testing';
import { AuthApiKeyService } from './auth-api-key.service';

describe('AuthApiKeyService', () => {
  let service: AuthApiKeyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthApiKeyService],
    }).compile();

    service = module.get<AuthApiKeyService>(AuthApiKeyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
