import { Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { HeaderAPIKeyStrategy } from 'passport-headerapikey'
import { authApiKey } from '../guard/api-key.guard'
import { fofEnv } from '../configuration'
import { EntAuthUsers, UsersService } from '@fof-nestjs/core'
import { performanceHelper } from '@fof-nestjs/core'

// https://github.com/hydra-newmedia/passport-headerapikey#readme

@Injectable()
export class apiKeyStrategy extends PassportStrategy(HeaderAPIKeyStrategy, authApiKey) {
  constructor(    
    private usersService: UsersService
  ) {    
    super({
      ...apiKeyStrategy.configSet()    
    }, true, async (apikey: string, done: any, req: any ) => { 
      performanceHelper.tick(req, 'API Key authentication')
      const user = await this.usersService.getUserAndAuthorizationsBySearch({apiToken: apikey})
      if (user) {
        return done(null, user)
      }
      return done(null, false)
    })    
  }

  // bypass the super must be the first intruction 
  private static configSet() {
    const conf = fofEnv()      
    return { header: conf.authApiKey.header, prefix: conf.authApiKey.prefix }
  } 

  async validate(req:any):Promise<any> {
    
  }
}

