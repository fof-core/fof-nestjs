// import { GlobalExchange } from '@fof-nestjs/core'
// GlobalExchange.authModules.add('auth-api-key')
// console.log(GlobalExchange.authModules.all())

export * from './auth-api-key.module'
export * from './auth-api-key.service'
export * from './guard/api-key.guard'
export * from './configuration'
