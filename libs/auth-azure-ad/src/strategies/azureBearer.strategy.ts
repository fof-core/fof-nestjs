import { Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { BearerStrategy } from 'passport-azure-ad'
import { fofEnv } from '../configuration'
import { azureAuthStringGuard } from '../guard/azure-ad.guard'
import { adUser } from '../entities/user.entity'
import { AzureAdService } from '../azure-ad.service'
import { performanceHelper } from '@fof-nestjs/core'

@Injectable()
export class AzureBearerStrategy extends PassportStrategy(BearerStrategy, azureAuthStringGuard) {
  constructor(    
    private azureAdService: AzureAdService,    
  ) {    
    super({
      ...AzureBearerStrategy.configSet()    
    })
  }

  // bypass the super must be the first intruction 
  private static configSet() {
    const conf = fofEnv()    
    let settings = {
      identityMetadata: conf.authAzure.identityMetadata,      
      clientID: conf.authAzure.clientID,      
      loggingLevel: conf.authAzure.loggingLevel,     
      loggingNoPII: conf.authAzure.loggingNoPII,      
      scope: conf.authAzure.scope,
      validateIssuer:false, 
      passReqToCallback: true,     
      //frontend bug ? should be client id by default (specification)
      audience: `api://${conf.authAzure.clientID}`
    }   
    return settings
  }
  
  async validate(req:any, adUser: adUser):Promise<any> {                
    /* 
      If this function is fired, it means the token is already validated by Azure B2C
      Now, we need to transform the OIDC user into a functionnal one      
    */
    performanceHelper.tick(req, 'Azure AD authentication')

    const user = await this.azureAdService.getOrCreateCurrentUserFromDb(adUser)
    if (user) {
      return user
    }
    return false
  }
}

