import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { EntAuthUsers } from '@fof-nestjs/core'
import { Repository } from 'typeorm'
import { adUser } from './entities/user.entity'
import { UsersService } from '@fof-nestjs/core'

@Injectable()
export class AzureAdService {

  constructor(
    @InjectRepository(EntAuthUsers) private userRepo: Repository<EntAuthUsers>,
    private usersService: UsersService
  ) {}

  async getOrCreateCurrentUserFromDb(adUser: adUser): Promise<EntAuthUsers> {   
    /**
      It must be the lightest possible 'cause it's called for eahc API request 
      Ideally, transformation should be cached (in core package?)
     */
    const getUserfromDB = async () => {
      // it should return everything and the core app has to select what it needs?
      return await this.userRepo.createQueryBuilder('user')
      // .select([
      //   'user.id',
      //   'user.firstName',
      //   'user.lastName',
      //   'user.eMail',      
      // ])
      // email is our unique ID for linking app user with Azure OIDC user
      .where('user.eMail = :eMail', { eMail: adUser.unique_name })
      .getOne()
    }

    let userDB = await getUserfromDB() 

    //The user doesn't exist in BDD, this is its first connection    
    if (!userDB) {
      // creation the user into the database with default access rights (should be set in package config)
      const result = await this.userRepo.createQueryBuilder()
      .insert()
      .values({
        firstName: adUser.given_name,
        lastName: adUser.family_name,
        eMail: adUser.unique_name
      })
      .execute()
      //toDo: test the result to verify the insert 
      //A general funtion should be created to test each insert/delete etc 
      
      // Now, we can get the user from db
      // userDB = await getUserfromDB()
    }

    return await this.usersService.getUserAndAuthorizationsBySearch({eMail: adUser.unique_name})

    // toDO: RBAC permission mecanism. 
    // Should get all permissions associated with a user by its roles for expl.
    // userDB['permissions'] = ['test']
    
    // return userDB
  }
}
