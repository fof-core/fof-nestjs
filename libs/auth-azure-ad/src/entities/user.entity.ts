import { ApiProperty } from "@nestjs/swagger"

/** Have a look on user AD for more detail */
export class adUser {  
  @ApiProperty({
    description: `aud: 'e.g. api://eb3e5b4e-c847-4a1b-9f47-51c55da9e577'`    
  })  
  aud: string
  @ApiProperty({description: `e.g. iss: 'https://sts.windows.net/984d4104-09b9-42e5-a80f-a48a1430c929/'`})  
  iss: string
  @ApiProperty({description: `e.g. iat: 1615471036`})    
  iat: string
  // nbf: 1615471036
  nbf: string
  // exp: 1615474936
  exp: string
  // acr: '1'
  acr: string
  // aio: 'AVQAq/8TAAAA36yHq+POp8laRLu+zoCmFGunsT0cvCgUyR7GSfTutH+GBOI3fXHMDlKBGL6TuB7IrHIvcZzJZgbypl3JSfVbVszaE4bKHCk8uHgugnQfFEM='
  aio: string
  @ApiProperty({ 
    description: `e.g. amr: [ 'pwd', 'rsa', 'mfa' ]`,
    type: [String] 
  })  
  amr: string[]
  // appid: 'eb3e5b4e-c847-4a1b-9f47-51c55da9e577'
  appid: string
  // appidacr: '0'
  appidacr: string
  // deviceid: '3ff9d10e-0bd0-4610-9baf-b699a836e042'
  deviceid: string
  @ApiProperty({description: `e.g. family_name: 'Fourteau'`})      
  family_name: string  
  @ApiProperty({description: `e.g. given_name: 'Frédéric'`})      
  given_name: string
  // ipaddr: '128.90.23.160'
  ipaddr: string  
  @ApiProperty({description: `e.g. name: 'Frédéric Fourteau`})    
  name: string
  // oid: 'a090a586-c1d4-4195-ba91-2aa42fd15d8b'
  oid: string
  // rh: '0.AQIABEFNmLkJ5UKoD6SKFDDJKU5bPutHyBtKn0dRxV2p5XcCAN8.'
  rh: string
  // scp: 'CvGen.Access'
  scp: string
  // sub: 'FeOZ_PCje4Mj3akkXVYNn7ZElPUc7NTMfkx2DXsX-FU'
  sub: string
  // tid: '984d4104-09b9-42e5-a80f-a48a1430c929'
  tid: string
  // unique_name: 'ffourteau@redmind-technology.com'
  unique_name: string
  // upn: 'ffourteau@redmind-technology.com'
  upn: string
  // uti: 'kRLLqK4TwkyHJXady78TAA'
  // uti: 'kRLLqK4TwkyHJXady78TAA'
  uti: string
  // ver: '1.0'
  ver: string
}