import { Test, TestingModule } from '@nestjs/testing';
import { AzureAdService } from './azure-ad.service';

describe('AzureAdService', () => {
  let service: AzureAdService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AzureAdService],
    }).compile();

    service = module.get<AzureAdService>(AzureAdService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
