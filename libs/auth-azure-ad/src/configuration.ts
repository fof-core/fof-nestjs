/** To enable intellisense capability on env file*/
export interface efofConfig {
  authAzure: {
    identityMetadata: string
    clientID: string
    loggingLevel: string
    scope: string[]
    loggingNoPII: boolean
  }  
}

/** 
 * Transform env vars into typed config
 * Doc here: https://docs.nestjs.com/techniques/configuration
 */
export const fofEnv = ():efofConfig => ({
  authAzure: {
    identityMetadata: process.env.AUTH_AZURE_IDENTITY_METADATA,
    clientID: process.env.AUTH_AZURE_CLIENT_ID,
    loggingLevel: process.env.AUTH_AZURE_LOGGING_LEVEL,
    scope: (process.env.AUTH_AZURE_SCOPE || '').split(' '),
    loggingNoPII: process.env.AUTH_AZURE_LOGGING_NO_PII == 'true'
  }  
})



