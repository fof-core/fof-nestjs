import { Module, Logger, OnApplicationShutdown, OnModuleInit} from '@nestjs/common'
import { AzureAdService } from './azure-ad.service'
import { AzureBearerStrategy } from './strategies/azureBearer.strategy'
import { FofCoreModule } from '@fof-nestjs/core'
import { azureAuthStringGuard } from './guard/azure-ad.guard'
import { EntAuthUsers } from '@fof-nestjs/core'
import { TypeOrmModule } from '@nestjs/typeorm'

@Module({
  imports: [
    FofCoreModule,
    TypeOrmModule.forFeature([      
      EntAuthUsers
    ])
  ],
  providers: [
    AzureAdService,
    AzureBearerStrategy
  ],
  exports: [
    AzureAdService
  ],
})
export class AzureAdModule implements OnApplicationShutdown, OnModuleInit {
  private readonly logger = new Logger('FofAuthAzureAD') 

  constructor() {
  
  }

  async onModuleInit () {    
    this.logger.log(`Add Azure AD authenticaton strategy.`)
    // console.log(fofEnv())
  }

  async onApplicationShutdown() {
    //todo
  }  

}
