import { Column, CreateDateColumn, UpdateDateColumn, VersionColumn, 
  BeforeInsert, PrimaryGeneratedColumn, PrimaryColumn, Generated, Index } from 'typeorm'
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger'
import { v4 as uuid } from 'uuid'
import { Reflector } from '@nestjs/core'


/**
 * Options for permissions
 
 * RBAC contextual authorization mecanism
 */

export interface entityOptions {
  organizationCheck: boolean
}

/** 
 * Base class for typeORM entity if you want to use the CRUD functionnality of fofCore
 * 
 * That class should be used only for many-to-many relationship table
 * 
 * For regular one with ID, use CrudEntity
 * 
 * That base object will manage
 * - security RBAC mecanism (automatic check of the organizationId vs permission)
 * - update automaticaly the technical fields (_createDate, _createdBy, etc) 
 * 
 * */
export abstract class CrudEntityWithoutId {  
  /** 
   * 
   * permissionRead
  */ 
  static permissionRead?: string = undefined
  @ApiHideProperty()
  @ApiHideProperty()
  /** @inheritdoc */
  static permissionCreate?: string = undefined
  @ApiHideProperty()
  static permissionUpdate?: string = undefined  
  @ApiHideProperty()
  static permissionDelete?: string = undefined
  @ApiHideProperty()
  static nameSingular?: string = undefined
  @ApiHideProperty()
  static namePlural?: string = undefined

  @ApiHideProperty()
  organizationId?: string = undefined  
 
  @ApiHideProperty()
  @CreateDateColumn()
  _createdDate?: Date

  @ApiHideProperty()
  @Column({nullable: true})
  _createdById?: string

  @ApiHideProperty()
  @UpdateDateColumn({})
  _updatedDate?: Date

  @ApiHideProperty()
  @Column({nullable: true })
  _updatedById?: string  
}

/** 
 * Base class for typeORM entity if you want to use the CRUD functionnality of fofCore
 * 
 * That class should be used only for a main entity
 * 
 * For many-to-many relationship entity, use CrudEntityWithoutId
 * 
 * That base object will manage
 * - security RBAC mecanism (automatic check of the organizationId vs permission)
 * - update automaticaly the technical fields (_createDate, _createdBy, etc) 
 * 
 * */
export abstract class CrudEntity extends CrudEntityWithoutId { 
  @ApiProperty({
    readOnly: true,
    description: 'Unique Id of the entity.',
    example: '48f0bdeb-b7ec-45b7-a7bd-d27f49199a51'
  })
  @ApiHideProperty()
  @PrimaryGeneratedColumn('uuid')  
  @Generated('uuid')
  id: string

  @ApiProperty({
    description: `Entity's external Unique ID.<br>
    That's UID is up to you and can be used to synchronize the structure with an external app.<br>
    !! Don't mistke it with the internal ID (read only) property`,
    example: '1234'  
  })  
  @Index()
  @Column({nullable: true})
  _externalUID?: string
}
