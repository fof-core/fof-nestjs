import { createParamDecorator, Get, HttpStatus, SetMetadata } from '@nestjs/common'
import { ApiBody, ApiOperation, ApiQuery, ApiResponse } from '@nestjs/swagger'
import { DtoError } from '../core/all-exceptions.filter'
import { eOuput, iGetManyParams } from './crud.interfaces'
import { getManyDtoFactory, iCrudControllerOption } from './crud.controller.factory'
import { CrudEntity } from './crud.entity'
import { applyDecorators } from '@nestjs/common'
import { SwaggerEnumType } from '@nestjs/swagger/dist/types/swagger-enum.type'
import { eCrudMethod } from './crud.service'

/**
 * Decorator that gets a crud search from the query request 
 * 
 * @publicApi 
 */
export const GetManyParams = createParamDecorator(  
  (data: unknown, ctx: any):iGetManyParams => {   
    const request = ctx.switchToHttp().getRequest()    
    return {
      ...request.query,
      user: request.user
    }
  }
)
/**
 * Decorator that define a create one with relation instance of the entity, openAPI documentation only.
 * 
 * !NestJs decorator should be used in addition (POST with desired path)
 * 
 * @publicApi    
 */
export function  CreateOneRelationMethod(options: {
  entityMain?: CrudEntity | any,  
  entityDto?: CrudEntity | any,  
  summary?: string,
  description?: string
}) {
  return (
    target: object,
    key?: string | symbol,
    descriptor?: TypedPropertyDescriptor<any>
  ): any => {  
    
    const plural = options.entityDto.namePlural || 'NO_NAME'
    const singular = options.entityDto.nameSingular || 'NO_NAME'

    ApiOperation({
      summary: options.summary || `Create one ${singular}`, 
      description: options.description || `Create one ${singular} and return it with its ID`
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.CREATED, description: `return the created ${singular} with its ID`, 
      type: options.entityDto 
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.BAD_REQUEST, description: `detailed error (or not.. relative to the user profile)`, type: DtoError
    })(target, key, descriptor)    
  }
}
/**
 * Decorator that define a delete one instance of the entity, openAPI documentation only.
 * 
 * !NestJs decorator should be used in addition (POST with desired path) *   
 * 
 * @publicApi 
 */
export function  DeleteOneRelationMethod(options: {  
  entityMain?: CrudEntity | any,  
  entityDto?: CrudEntity | any,  
  summary?: string,
  description?: string
}) {
  return (
    target: object,
    key?: string | symbol,
    descriptor?: TypedPropertyDescriptor<any>
  ): any => {    
    const plural = options.entityDto.namePlural || 'NO_NAME'
    const singular = options.entityDto.nameSingular || 'NO_NAME'

    ApiOperation({
      summary: options.summary || `Delete one ${singular}`, 
      description: options.description || `Delete one ${singular} by its ID.<br>`
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.OK, description: `${singular} successfully deleted`, type: options.entityDto 
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.NOT_FOUND, description: `NotFound` 
    })(target, key, descriptor)    
  }
}

export function GetManyCore(options: {
  entityMain?: CrudEntity | any,  
  entityDto?: CrudEntity | any,  
  summary?: string,
  description?: string,
  views?: SwaggerEnumType
}) {
  const plural = options.entityDto.namePlural || 'NO_NAME'
  const singular = options.entityDto.nameSingular || 'NO_NAME'
  const manyDto = getManyDtoFactory(options.entityDto, singular)

  // console.log('plural', plural)
  // console.log('singular', singular)
  // console.log('plural', plural)

  let viewsHelp = ``

  if (options.views) {
    viewsHelp = `Available views<br>`    
    for (const view in options.views) {      
      viewsHelp += `- ${view}<br>`
    }
  }

  return applyDecorators(    
    ApiResponse({ 
      status: HttpStatus.OK, description: `get many ${plural}`, type: manyDto, isArray: false
    })       ,
    ApiResponse({ 
      status: HttpStatus.NOT_FOUND, description: `NotFound` 
    }),  
    ApiQuery({ 
      name: 'page', 
      description: `The current page you want to display<br>
      default is 1.
      `, 
      required: false, type: Number 
    }),
    ApiQuery({      
      name: 'limit', 
      description: `Number or records per page<br>
      default is 100`, 
      required: false, type: Number 
    }),  
    ApiQuery({ 
      name: 'select', 
      description: `List of fields to return.<br>
      the FQDN of each field should be set. e.g. ${plural}.id<br>
      <br>
      Usage<br>
      - fieldName1,fieldName2`, 
      required: false 
    }),     
    ApiQuery({ 
      name: 'filter', 
      description: `filter conditions<br>
      <br>
      The FQDN of each field should be set. e.g. ${plural}.id<br>
      <br>
      Operators <br>
      - EXACT: $eq <br>
      - NOT: ! (use "!" before an operator. e.g. fieldName1||!$eq||value) <br>
      - CONTAINS: $cont <br>
      - IS_NULL: $isnull (e.g. fieldName||!$isnull -> fieldName is not null)<br>
      - GT: $gt (>) <br>
      - GTE: $gte (>=) <br>
      - LT: $lt (<) <br>
      - LTE: $lte (<=) <br>
      - STARTS_WITH: $starts <br>
      - ENDS_WITH: $ends <br>
      - IN: $in (separate the values with a comma. e.g. id||$in||1,2,3,4) <br>
      - BETWEEN: $between (separate the values with a comma. e.g. fieldName||$between||value1,value2) <br>   
      Usage<br>
      - AND: fieldName1||operator||value;fieldName2||operator||value <br>
      - OR: fieldName1||operator||value||$or||fieldName2||operator||value <br>
      - Mix both: fieldName1||operator||value;fieldName2||operator||value||$or||fieldName3||operator||value <br>
          -> (fieldName1 = value AND fieldName2 = value) OR (fieldName3 = value )<br>
      <br>
      `,
      required: false 
    }),
    ApiQuery({ 
      name: 'sort', 
      description: `Name of the field to sort by<br>
      <br>      
      Operators <br>
      - ASC <br>
      - DESC <br>
      <br>
      Usage<br>
      - fieldName1,ASC;fieldName2,DESC`, 
      required: false 
    }),     
    ApiQuery({      
      name: 'output', 
      description: `desired output format<br>
      Default is json<br>`,
      enum: eOuput, 
      required: false, type: String 
    }),  
    ApiQuery({      
      name: 'view', 
      description: `run another view<br>      
      Some endpoints may expose additional views relative to the entity.<br>
      <br>
      ${viewsHelp}<br><br>
      !important: be aware the pagination functionnality is not relevant with views. <br>      
      All filters, sort etc features are available for the view as well`, 
      required: false, type: String 
    }),
    ApiQuery({      
      name: 'extras', 
      description: `Additionnal information<br><br>
      available <br>
      - metrics <br>
      - query <br>
      - help <br><br>
      e.g. metrics, query, help`, 
      required: false, type: String      
    })
  )
}

/**
 * Decorator that define get many entities in relation with entity endpoint, openAPI documentation only.
 * 
 * !NestJs decorator should be used in addition (POST with desired path) 
 * 
 * @publicApi 
 */
export function  GetManyRelationMethod(options: {
  entityMain?: CrudEntity | any,  
  entityDto?: CrudEntity | any,  
  summary?: string,
  description?: string
}) {
  return (
    target: object,
    key?: string | symbol,
    descriptor?: TypedPropertyDescriptor<any>
  ): any => {
    const plural = options.entityDto.namePlural || 'NO_NAME'
    const singular = options.entityDto.nameSingular || 'NO_NAME'
    const manyDto = getManyDtoFactory(options.entityDto, singular)

    ApiOperation({
      summary: options.summary || `Get many ${plural}`, 
      description: options.description ||`Get many ${plural} according to the query`
    })(target, key, descriptor)
    GetManyCore(options)(target, key, descriptor)  
  }
}
/**
 * Decorator that define a create bulk endpoint for an entity in relation with the main one, openAPI documentation only.
 * 
 * !NestJs decorator should be used in addition (POST with desired path) 
 * 
 * @publicApi 
 */
export function  CreateBulkRelationMethod(options: {
  entityDto?: CrudEntity | any, 
  entityMain?: CrudEntity | any,   
  summary?: string,
  description?: string
}) {
  return (
    target: object,
    key?: string | symbol,
    descriptor?: TypedPropertyDescriptor<any>
  ): any => {  
    const plural = options.entityMain.namePlural || 'NO_NAME'
    const singular = options.entityMain.nameSingular || 'NO_NAME'
    
    ApiOperation({
      summary: options.summary || `Create many ${plural}`, 
      description: options.description || `Create many ${plural} in ones and return ?ok?`
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.CREATED, description: `tbd`, type: options.entityDto 
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
    })(target, key, descriptor)
    ApiResponse({ 
      status: HttpStatus.BAD_REQUEST, description: `detailed error (or not.. relative to the user profile)`, type: DtoError
    })(target, key, descriptor)
    ApiBody({ 
      type: [options.entityDto] 
    })(target, key, descriptor)
    ApiBody({ 
      type: [options.entityDto] 
    })(target, key, descriptor)    
  }
}

// export function Crud(mandatory: {create: string}) {  
//   return applyDecorators(
//     SetMetadata('mandatory', mandatory)        
//   )
// }

export function  Crud(options: {  
  summary?: string,
  description?: string
}) {
  return (
    target: object,
    key?: string | symbol,
    descriptor?: TypedPropertyDescriptor<any>
  ): any => {
    Reflect.metadata('summary', options.summary) 
  }
}

