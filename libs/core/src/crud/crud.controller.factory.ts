import { FofCrudService, eCrudMethod } from './crud.service'
import { applyDecorators, Body, Delete, Get, HttpStatus, Param, Patch, Post, Put, Req, Res, StreamableFile, Response } from '@nestjs/common'
import { GetManyParams } from './crud.decorators'
import { eOuput, iGetManyParams, iGetManyResult } from './crud.interfaces'
import { performance } from 'perf_hooks'
import { ApiBody, ApiOperation, ApiProperty, ApiQuery, ApiResponse } from '@nestjs/swagger'
import { CrudEntity } from './crud.entity'
import { fofEnv, efofConfig } from '../config/configuration'
import { FofPerfLog } from '../decorators/core.decorators'
import { performanceHelper, iFofPerf } from '../core/logger.middleware'
import { SwaggerEnumType } from '@nestjs/swagger/dist/types/swagger-enum.type'
import { View } from 'typeorm/schema-builder/view/View'
import { FofUser } from '../decorators/core.decorators'
import { EntAuthUsers } from '../permissions/users/users.entity'
import { GetManyCore } from './crud.decorators'

export const getManyDtoFactory = (dto: any, resourceName: string): any => {
  class GetManyResponseDto extends  iGetManyResult {
    @ApiProperty({
      isArray: true, 
      type: dto
    })     
    data: typeof dto[]
  }

  Object.defineProperty(GetManyResponseDto, 'name', {
    writable: false,
    value: `GetMany${resourceName.charAt(0).toUpperCase() + resourceName.slice(1)}ResponseDto`,
  })

  return GetManyResponseDto
}

export class DtoError {
  // @ApiProperty({type: ErrorResponse})
  // response: ErrorResponse  
  @ApiProperty({example: 400})
  status: string
  @ApiProperty({example: 'malformed entity'})
  message: string
  @ApiProperty({example: 'BadRequestException'})
  name: string
  @ApiProperty()
  response: any
}

/** Options for the documentation creation of that controller */
export interface iCrudControllerOption {
  /** That options object should be imported from the relative service of the controller.*/
  // serviceOptions?: CrudServiceOptions,
  /** Keep only this enpoint. 
   * 
   * The list can be imported from the enum eCrudMethod */
  endpointsKeepOnly?: string[],
  entityDto?: CrudEntity | any,
  entityMain?: CrudEntity | any,  
  views?: SwaggerEnumType
}

const globalEnv = fofEnv()

export const crudControllerFactory = <Entity>(options: iCrudControllerOption):any => {

  const plural = options.entityMain.namePlural || 'NO_NAME'
  const singular = options.entityMain.nameSingular || 'NO_NAME'
  let viewsHelp = ``

  if (options.views) {
    viewsHelp = `Available views<br>`    
    for (const view in options.views) {      
      viewsHelp += `- ${view}<br>`
    }
  }

  const manyDto = getManyDtoFactory(options.entityDto, singular)
  
  @ApiResponse({ 
    status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
  })
  class CrudController {
    constructor(
      public service: FofCrudService<Entity>
    ) {
      this.env = fofEnv()      
    }    

    private env: efofConfig    

    @ApiOperation({
      summary: `Get many ${plural}`,     
      description: `Get many ${plural} according to the query`
    })    
    @GetManyCore(options)
    @Get()
    async getMany(@FofPerfLog() perfs: iFofPerf[], @GetManyParams() query: iGetManyParams):Promise<iGetManyResult> {
      const result =  await this.service.getMany(query, {fofPerf: perfs})
      // // if expost csv or xlxs is asked
      // toDo: manage filename result
      // // @Res() res: Response
      // if (result instanceof StreamableFile) {        
      //   console.log('result', result)          
      // }      
      return performanceHelper.decorateResult<iGetManyResult>(perfs, result)
    }

    @ApiOperation({
      summary: `Find one ${singular}`, 
      description: `Find one ${singular} by its ID`
    })
    @ApiResponse({ 
      status: HttpStatus.OK, 
      description: `${singular} found`, 
      type: options.entityDto 
    })
    @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: `NotFound` })
    @Get(':id')  
    async findOneById(@Param('id') id: string, @FofUser() user: EntAuthUsers):Promise<Entity> {
      return this.service.findOneById(id, user)
    }
    
    @ApiOperation({
      summary: `Create one ${singular}`, 
      description: `Create one ${singular} and return it with its ID`
    })
    @ApiResponse({ 
      status: HttpStatus.CREATED, description: `return the created ${singular} with its ID`, type: options.entityDto 
    })
    @ApiResponse({ 
      status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
    })
    @ApiResponse({ 
      status: HttpStatus.BAD_REQUEST, description: `detailed error (or not.. relative to the user profile)`, type: DtoError
    })
    @ApiBody({ 
      type: options.entityDto
    })
    @Post() 
    async createOne(@Body() dto: any, @FofUser() user: EntAuthUsers):Promise<Entity> {
      return this.service.createOne(dto, user)
    }

    @ApiOperation({
      summary: `Create many ${plural}`, 
      description: `Create many ${plural} in ones and return ?ok?`
    })
    @ApiResponse({ 
      status: HttpStatus.CREATED, description: `tbd`, type: options.entityDto 
    })
    @ApiResponse({ 
      status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
    })
    @ApiResponse({ 
      status: HttpStatus.BAD_REQUEST, description: `detailed error (or not.. relative to the user profile)`, type: DtoError
    })
    @ApiBody({ 
      type: [options.entityDto] 
    })
    @Post('bulk') 
    async createBulk(@Body() dto: any[], @FofUser() user: EntAuthUsers):Promise<boolean> {
      return this.service.createBulk(dto, user)
    }
  
    @ApiOperation({
      summary: `Update one ${singular}`, 
      description: `Get one ${singular} by its ID and update it.<br>
      To delete one field from the database, send null as its value.`
    })
    @ApiResponse({ 
      status: HttpStatus.OK, description: `return the modified ${singular}`, type: options.entityDto 
    })
    @ApiResponse({ 
      status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
    })
    @ApiResponse({ 
      status: HttpStatus.BAD_REQUEST, description: `detailed error (or not.. relative to the user profile)`, type: DtoError
    })
    @ApiResponse({ 
      status: HttpStatus.NOT_FOUND, description: `NotFound` 
    })
    @ApiBody({ 
      type: options.entityDto
    })
    @Patch(':id')
    async updateOneById(@Param('id') id: string, @Body() dto: Entity, @FofUser() user: EntAuthUsers):Promise<Entity> {
      return this.service.updateOneById(id, dto, user)
    }
  
    @ApiOperation({
      summary: `Replace one ${singular}`, 
      description: `Get one ${singular} by its ID and replace by the sent one.<br>`
    })
    @ApiResponse({ 
      status: HttpStatus.OK, description: `return the modified ${singular}`, type: options.entityDto 
    })
    @ApiResponse({ 
      status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
    })
    @ApiResponse({ 
      status: HttpStatus.BAD_REQUEST, description: `detailed error (or not.. relative to the user profile)`, type: DtoError
    })
    @ApiResponse({ 
      status: HttpStatus.NOT_FOUND, description: `NotFound` 
    })
    @ApiBody({ 
      type: options.entityDto
    })
    @Put(':id')
    async replaceOneById(@Param('id') id: string, @Body() dto: Entity, @FofUser() user: EntAuthUsers):Promise<Entity> {
      return this.service.replaceOneById(id, dto, user)
    }
  
    @ApiOperation({
      summary: `Delete one ${singular}`, 
      description: `Delete one ${singular} by its ID.<br>`
    })
    @ApiResponse({ 
      status: HttpStatus.OK, description: `${singular} successfully deleted`, type: options.entityDto 
    })
    @ApiResponse({ 
      status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` 
    })
    @ApiResponse({ 
      status: HttpStatus.NOT_FOUND, description: `NotFound` 
    }) 
    @Delete(':id')
    async deleteOneById(@Param('id') id: string, @FofUser() user: EntAuthUsers):Promise<any> {
      return this.service.deleteOneById(id, user)
    }
    
  }
  // remove unused endpoint. Nothing better for now...
  if (options.endpointsKeepOnly) {
    for (const endPoint in eCrudMethod) {
      if (!options.endpointsKeepOnly.includes(endPoint)) {
        CrudController.prototype[endPoint] = null
      }  
    }    
  }
  return CrudController
}