import { BadRequestException, HttpException, HttpStatus, NotFoundException, StreamableFile, UnauthorizedException } from '@nestjs/common'
import { Brackets, ObjectLiteral, SelectQueryBuilder, Repository, Entity, getConnection, EntityMetadata, getRepository } from 'typeorm'
import { EntAuthUsers } from '../permissions/users/users.entity'
import { iGetManyOptions, iGetManyParams, iGetManyResult } from './crud.interfaces'
import { QueryHelper, iParsedParams } from './query-helper'
import { InjectRepository } from '@nestjs/typeorm'
import { isUUID, validate, validateOrReject, ValidationError } from 'class-validator'
import { plainToClass } from 'class-transformer'
import { CrudEntity, CrudEntityWithoutId } from './crud.entity'
import { eErrorType } from '../core/all-exceptions.filter'
import { performanceHelper } from '../core/logger.middleware'
import { BehaviorSubject, Observable } from 'rxjs'
import { vOrganizationsPermissions } from '../permissions/permissions.views'
import { Stream } from 'stream'


type ClassType<Entity> = {
  new (...args: any[]): Entity
}

type QueryType = () => SelectQueryBuilder<CrudEntity>

export class iView {
  name: string  
  query: QueryType
}

export enum ePermissionCheck {
  permissionCreate = 'permissionCreate',
  permissionRead = 'permissionRead',
  permissionUpdate = 'permissionUpdate',
  permissionDelete = 'permissionDelete'
}

export enum eCrudMethod {
  createOne = 'createOne',
  findOneById = 'findOneById',
  updateOneById = 'updateOneById',
  replaceOneById = 'replaceOneById',
  deleteOneById = 'deleteOneById',
  getMany = 'getMany',  
  createBulk = 'createBulk'
}

export interface crudServiceOptions {
  forbiddenFields?: string[]
}

export abstract class FofCrudService<Entity> {

  constructor (   
    protected readonly repo: Repository<Entity>    
  ) {
    this._nameSingular = (this.entityType as any).nameSingular
    this._namePlural = (this.entityType as any).namePlural

    this.notifUpdataOne = new BehaviorSubject<any>(null)
    this.notifUpdataOne$ = this.notifUpdataOne.asObservable()
  }

  abstract options: crudServiceOptions

  public get namePlural() : string {
    return this._namePlural 
  }
  public get nameSingular() : string {
    return this._nameSingular 
  }  
  protected get entityType(): ClassType<Entity> {
    return this.repo.target as ClassType<Entity>
  }

  private _nameSingular!: string
  private _namePlural!: string
  private _views: iView[] = []
  public mainSearch: SelectQueryBuilder<Entity>

  private notifUpdataOne: BehaviorSubject<any>;
  public notifUpdataOne$: Observable<any>

  protected helpers = {
    validateDto: async(dto: Entity):Promise<CrudEntityWithoutId> => {
      let typedDto:CrudEntityWithoutId
      try {
        typedDto = plainToClass(this.entityType, dto)      
        await validateOrReject(typedDto)
      } catch (error) {      
        throw new BadRequestException(error, eErrorType.validationError)             
      } 
      return typedDto
    },
    validateUUID: async(id: string) => {
      if (!isUUID(id, '4')) {
        throw new BadRequestException(eErrorType.invalidUID)        
      }
    },
    getViewByName:(viewName: string):SelectQueryBuilder<any> | null => {
      const views = this._views.filter(v => v.name.toLowerCase() == viewName.trim().toLowerCase())
      if (views.length > 0) {
        return <any>views[0].query()
      }
      return null
    }
  }

  private ifFunctionImplemented (functionName: string, ...args: any) {
    if(typeof this[functionName] === "function") {
      this[functionName](args)
    }    
  }

  /** 
   * Allow to set an intity with permissions but without
   * a link to the organization.
   * */
  public disableRBACOrganizationCheck = false

  public async addView(relation: iView) {
    this._views.push(relation)
  }

  /**
   * Check an entity which is a part of the RBAC mecanism
   * 
   * It means the entity has the RBAC security implemented:
   * - an organizationId which allow to check tthe access right relative to the user
   * - the crud permission set (permissionCreate, permissionRead, etc)
   * 
   * !!! Call that method after having managed all your operation with the dto
   * - adapt some fields
   * - remove others, etc
   * You must check the final dto JUST BEFORE saving or returning it
   * 
   * @param userId user who is doing the operation
   * @param permissionToCheck the ePermissionCheck you want to check
   * @param dto the dto JUST before saving or returning it if it's a read permissions
   * @returns nothing to do with: The method will fire an exception if necessary
   */
  protected async throwUnauthorizedIfNotAllowed(user: EntAuthUsers, permissionToCheck: ePermissionCheck, dto?: CrudEntity | any): Promise<boolean> {
    let permissionCode: string
    let organizationId: string

    permissionCode = this.entityType[permissionToCheck]
    organizationId = dto.organizationId

    // it means the entity doesn't have RBAC to check
    if (!permissionCode) { return true }    
    // organizationId is mandatory for RBAC mecanism context
    // but some entities can use it without that context    
    if (this.disableRBACOrganizationCheck) { 
      // check if the user have permissions without context
      if (!user.permissions || !user.permissions.includes(permissionCode)) { throw new UnauthorizedException() }
      return true 
    }
    // At that point, all instances should have an organizationId
    if (!organizationId) { throw new UnauthorizedException() }    

    const userAllowedOrganizations:vOrganizationsPermissions[] = 
        await getRepository(vOrganizationsPermissions)
          .createQueryBuilder('vo')  
          .select('vo.organizationChildId')
          .where('vo.userId = :id', { id: user.id })
          .andWhere(`vo.permissionCode = :permissionCode`, {permissionCode: permissionCode})    
          .andWhere('vo.organizationChildId =:organizationId', {organizationId: organizationId})        
          .getMany() 
    
    if (userAllowedOrganizations.filter(op => op.organizationChildId == dto.organizationId).length > 0) {
      return true
    }
    throw new UnauthorizedException()
  }

  public async createOne(dto: Entity, user: EntAuthUsers):Promise<Entity> {    
    await this.helpers.validateDto(dto)       
    await this.throwUnauthorizedIfNotAllowed(user, ePermissionCheck.permissionCreate, dto)
    dto['_createdById'] = user.id
    return await this.repo.save(dto)    
  }

  public async createBulk(dtos: Entity[], user: EntAuthUsers):Promise<boolean> {
    // toDO: not efficient at all for large bulk! 
    if (dtos && dtos.length > 0) {
      for (let index = 0; index < dtos.length; index++) {
        const dto = dtos[index]
        dto['_createdById'] = user.id
        await this.throwUnauthorizedIfNotAllowed(user, ePermissionCheck.permissionCreate, <any>dto)
      }
    }
    const result = this.repo.createQueryBuilder()
      .insert()
      .values(dtos)
      .execute()
    
    if (result) { return true }
    return false
  }

  public async findOneById(id: string, user: EntAuthUsers):Promise<Entity> {
    await this.helpers.validateUUID(id)
    const found = await this.repo.findOne(id)    
    if (!found) { throw new NotFoundException() }
    await this.throwUnauthorizedIfNotAllowed(user, ePermissionCheck.permissionRead, found)
    return found    
  }

  public async updateOneById(id: string, dto: Entity, user: EntAuthUsers) {
    await this.helpers.validateUUID(id)
    const found = await this.repo.findOne(id)    
    if (!found) { throw new NotFoundException() }

    // allow to delete a key when null is set to it    
    for (let key of Object.keys(dto)) {
      found[key] = dto[key]      
    }

    dto['_updatedById'] = user.id
    await this.helpers.validateDto(found)
    await this.throwUnauthorizedIfNotAllowed(user, ePermissionCheck.permissionUpdate, found)
    const saved = await this.repo.save(found)

    this.notifUpdataOne.next({
      // type: this.entityType,
      dto: saved
    })
    return saved
  }

  public async replaceOneById(id: string, dto: Entity, user: EntAuthUsers) {
    await this.helpers.validateUUID(id)
    const found = await this.repo.findOne(id)
    if (!found) { throw new NotFoundException() }
    
    for (let key of Object.keys(found)) {
      found[key] = dto[key]      
    }

    dto['_updatedById'] = user.id
    await this.helpers.validateDto(found)
    await this.throwUnauthorizedIfNotAllowed(user, ePermissionCheck.permissionUpdate, found)
    const saved = await this.repo.save(found)
    return saved
  }

  public async deleteOneById(id: string, user: EntAuthUsers) {    
    await this.helpers.validateUUID(id)
    const found = await this.repo.findOne(id)
    if (!found) { throw new NotFoundException() }
    await this.throwUnauthorizedIfNotAllowed(user, ePermissionCheck.permissionDelete, found)
    const deleted = await this.repo.delete(id)    
    return {
      deletedResult: deleted,
      deletedRecord: found
    }
  }

  public async getMany(query: iGetManyParams, options?: iGetManyOptions):Promise<iGetManyResult> {

    let search: SelectQueryBuilder<Entity>

    let defaultSearch : SelectQueryBuilder<Entity> = 
      this.repo.createQueryBuilder(this._namePlural)

    let permissionCode = this.entityType[ePermissionCheck.permissionRead]
    if (permissionCode && !this.disableRBACOrganizationCheck) {       
      defaultSearch = this.repo.createQueryBuilder(this._namePlural)
       // access management         
       .innerJoin('auth_v_organizations_permissions', 'op', `${this._namePlural}.organizationId = op.organizationChildId`)
       .andWhere('op.userId = :userId', {userId: query.user.id})
       .andWhere(`op.permissionCode = '${permissionCode}'`) 
    }

    if (query.view) {
      search = this.helpers.getViewByName(query.view)
    } 
    
    if (!search) {
      search = options.customQueryBuilder || defaultSearch
    }

    const qE:QueryHelper = new QueryHelper({
      query: query,
      queryBuilder: search,
      plural: this.namePlural,
      singular: this.nameSingular,
      serviceOption: this.options
    })

    performanceHelper.tick(options?.fofPerf, 'pre-processing')
    const result = await qE.queryBuilder.getManyWithPagination()
    performanceHelper.tick(options?.fofPerf, 'datastore call & orm processing')
    
    return result
  }

}