import { BadRequestException, HttpException, HttpStatus, StreamableFile } from '@nestjs/common'
import { Between, Brackets, IsNull, LessThan, LessThanOrEqual, Like, 
  MoreThan, MoreThanOrEqual, Not, SelectQueryBuilder, WhereExpression } from 'typeorm'
import { CrudEntity } from './crud.entity'
import { eExtras, eOuput, iGetManyParams, iGetManyResult } from './crud.interfaces'
import * as XLSX from 'xlsx'
import { crudServiceOptions } from './crud.service'

export interface IOptionsObject {
  LOOKUP_DELIMITER?: string
  RELATION_DELIMITER?: string
  EXACT?: string
  NOT?: string
  CONTAINS?: string
  IS_NULL?: string
  GT?: string
  GTE?: string
  LT?: string
  LTE?: string
  STARTS_WITH?: string
  ENDS_WITH?: string
  IN?: string
  BETWEEN?: string
  OR?: string
  CONDITION_DELIMITER?: string
  VALUE_DELIMITER?: string
  DEFAULT_LIMIT?: string
}
export interface IQueryTypeOrm {
  select?: string[]
  relations?: string[]
  where?: []
  order?: {}
  skip?: number
  take?: number
  cache?: boolean
}

interface ILooseObject {
  [key: string]: any
}

interface IWhere {
  field: string,
  operator: IOptionsObject,
  value: string,
  notOperator: string
}

interface iSort {
  field: string,
  order: string
}

export interface iParsedParams {
  select?: string[],
  where?: IWhere[][],
  sort?: iSort[],
  page?: number,
  limit?: number,
  extras?: string[],
  output?: eOuput,
  view?: string
}

export interface iQueryHelperOptions {
  query: iGetManyParams,  
  queryBuilder: SelectQueryBuilder<any>
  configuration?: IOptionsObject,
  plural?: string,
  singular?: string,
  serviceOption?: crudServiceOptions
}

const regExFieldNameOnly = /^[A-Za-z-._]+$/

export class QueryHelper {

  constructor(options?: iQueryHelperOptions) {
    this._options = {
      ...{
        LOOKUP_DELIMITER: '||',
        RELATION_DELIMITER: '.',
        CONDITION_DELIMITER: ';',
        VALUE_DELIMITER: ',',
        EXACT: '$eq',
        NOT: '!',
        CONTAINS: '$cont',
        IS_NULL: '$isnull',
        GT: '$gt',
        GTE: '$gte',
        LT: '$lt',
        LTE: '$lte',
        STARTS_WITH: '$starts',
        ENDS_WITH: '$ends',
        IN: '$in',
        BETWEEN: '$between',
        OR: '$or',
        DEFAULT_LIMIT: '100',
      },
      ...options?.configuration || {},
    }

    this._singular = options.singular
    this._plural = options.plural

    this.typeORMqueryBuilder = options.queryBuilder
    this._serviceOption = options.serviceOption
    
    const parsedParams = this.reqQueryParams.parseAll(options.query)
    // could change the this.typeORMqueryBuilder
    this._queryBuilder.viewSelect()
    this.queryBuilder.addSelectFromParams(this.typeORMqueryBuilder)
    this.queryBuilder.addWhereFromParams(this.typeORMqueryBuilder)        
    this.queryBuilder.addPaginationFromParams(this.typeORMqueryBuilder)
    this.queryBuilder.addSortFromParams(this.typeORMqueryBuilder)
  }

  protected _options: IOptionsObject
  private _serviceOption: crudServiceOptions
  protected _paramCpt = 0
  protected _parsedParams:iParsedParams
  private _plural: string
  private _singular: string
  
  public get options():IOptionsObject {
    return this._options 
  }  
  public get parsedParams():iParsedParams {
    return this._parsedParams
  } 

  public static addAllQueryParamsToQueryBuilder(query : {
    query: iGetManyParams,  
    queryBuilder: SelectQueryBuilder<CrudEntity>
  }): QueryHelper {

    const queryHelper = new QueryHelper()

    const parsedParams = queryHelper.reqQueryParams.parseAll(query.query)

    queryHelper.queryBuilder.addSelectFromParams(query.queryBuilder)
    queryHelper.queryBuilder.addWhereFromParams(query.queryBuilder)    
    // queryHelper.queryBuilder.AddRelationFromParams(queryBuilder)
    queryHelper.queryBuilder.addPaginationFromParams(query.queryBuilder)
    queryHelper.queryBuilder.addSortFromParams(query.queryBuilder)

    return queryHelper
  }

  private typeORMqueryBuilder: SelectQueryBuilder<CrudEntity>


  private _reqQueryParams = {
    isPopulated:(value: string | undefined): boolean => {
      if (!value) {
        return true
      }
      return false
    },
    parseWhere:(filterString: string): IWhere[][] => {
      const queryToAdd: IWhere[][] = []    
      const orArray = filterString.split(
        (this._options.LOOKUP_DELIMITER as string) + this._options.OR + this._options.LOOKUP_DELIMITER,
      )    
      orArray.forEach(item => {
        let obj = []
        const condition = item.split(this._options.CONDITION_DELIMITER as string)     
        const parsedCondition = condition.map(q => q.split(this._options.LOOKUP_DELIMITER as string))
        
        parsedCondition.forEach(cond => {
          let notOperator = false
          if (cond[1] && cond[1].startsWith(this._options.NOT as string)) {
            notOperator = true
            const index = (this._options.NOT as string).length;
            cond[1] = cond[1].slice(index)
          }

          this.helpers.throwErrorIfForbiddenField(cond[0].trim())
        
          obj.push({
            field: cond[0].trim(),
            operator: cond[1].trim(),
            value: cond[2]?cond[2].trim():'',
            notOperator: notOperator
          })      
        })         
        queryToAdd.push(obj)                   
      })
      
      return queryToAdd
    },
    parseSort:(filterString: string): iSort[] => {
      const sortConditions = filterString.split(this.options.CONDITION_DELIMITER as string)
      const sortResult: iSort[] = []
      const order: ILooseObject = {}
     
      sortConditions.forEach(condition => {
        const [key, value] = condition.split(this.options.VALUE_DELIMITER as string)      
        if (key) {
          let fieldName = key
          // enforce FQDN for fieldName
          if (fieldName.split('.').length < 2) {
            fieldName = `${this._plural}.${key}`
          }
          sortResult.push({
            field: fieldName,
            order: (value || 'ASC').toUpperCase()
          })        
        }
      })
      
      return sortResult
    }
  }  

  private _queryBuilder = { 
    viewSelect:() => {
      if (this._parsedParams.view) {
        // if (this._)
      }
    },
    addWhereToQueryBuilder:(param: IWhere, select: SelectQueryBuilder<CrudEntity> | WhereExpression) => {      
      this._paramCpt++      
  
      switch (param.operator) {
        case this._options.EXACT:
          select = select
            .andWhere(`${param.field} ${param.notOperator? '!': ''}= :${this._paramCpt}`, 
              {[this._paramCpt]: `${param.value}`})
          break
        case this._options.CONTAINS:
          select = select
            .andWhere(`${param.field} ${param.notOperator? 'NOT': ''} like :${this._paramCpt}`, 
              {[this._paramCpt]: `%${param.value}%`})
          break
        case this._options.IN:        
          select = select          
            .andWhere(`${param.field} ${param.notOperator? 'NOT': ''} IN (:...${this._paramCpt})`, 
              {[this._paramCpt]: param.value.split(',').map(v => v.trim())})
          break
        case this._options.IS_NULL:        
          select = select          
            if (!param.notOperator) {              
              select = select          
                .andWhere(`${param.field} IS NULL`)
            } else {
              select = select          
                .andWhere(`${param.field} IS NOT NULL`)
            }          
          break
        case this._options.STARTS_WITH:  
          select = select
            .andWhere(`${param.field} ${param.notOperator? 'NOT': ''} like :${this._paramCpt}`, {[this._paramCpt]: `${param.value}%`})        
          break
        case this._options.ENDS_WITH:          
          select = select
            .andWhere(`${param.field} ${param.notOperator? 'NOT': ''} like :${this._paramCpt}`, {[this._paramCpt]: `${param.value}%`})
          break
        case this._options.LT:     
          // not tested
          select = select
            .andWhere(`${param.field} ${param.notOperator? '>': '<'} :${this._paramCpt}`, {[this._paramCpt]: param.value})     
          break
        case this._options.LTE:
          // not tested
          select = select
            .andWhere(`${param.field} ${param.notOperator? '<': '>'} :${this._paramCpt}`, {[this._paramCpt]: param.value})           
          break
        case this._options.GT:  
          // not tested
          select = select
            .andWhere(`${param.field} ${param.notOperator? '=>': '<='} :${this._paramCpt}`, {[this._paramCpt]: param.value})             
          break
        case this._options.GTE: 
          // not tested
          select = select
            .andWhere(`${param.field} ${param.notOperator? '<=': '=>'} :${this._paramCpt}`, {[this._paramCpt]: param.value})         
          break        
        case this._options.BETWEEN:
          // not tested
          const values = param.value?.split(',').map(v => v.trim())
          if (values.length != 2) {
            const error = {
              error: `between operator need 2 values separated by a comma`,
              ...param
            }
            throw new HttpException(error, HttpStatus.BAD_REQUEST)
          }
          select = select
            .andWhere(`${param.field} ${param.notOperator? '>': '<'} :${this._paramCpt}`, {[this._paramCpt]: values[1]})     
            .andWhere(`${param.field} ${param.notOperator? '<': '>'} :${this._paramCpt}`, {[this._paramCpt]: values[0]})
        default:        
          const error = {
            error: `unknown operator`,
            ...param
          }
          throw new HttpException(error, HttpStatus.BAD_REQUEST)
          break
      }
  
      return select
    }
  }

  private helpers = {    
    getHumanReadableParsedParams:() => {

      let where: string = ''
      let sort = ''
      let firstOr = true

      // todo: finish switch list
      const cleanOperator = (operator: IOptionsObject, notOp: boolean | string) => {
        switch (operator) {
          case this._options.EXACT:
            if (notOp) { return 'NOT ='}
            return '='
            break           
          case this._options.CONTAINS:
            if (notOp) { return 'NOT CONTAINS'}
            return 'CONTAINS'
            break
          case this._options.STARTS_WITH:
            
            break;
          case this._options.ENDS_WITH:
            
            break;
          case this._options.IS_NULL:
            
            break;
          case this._options.LT:
            
            break;
          case this._options.LTE:
            
            break;
          case this._options.GT:
            
            break;
          case this._options.GTE:
            
            break;
          case this._options.IN:           
            break;
          case this._options.BETWEEN:            
            break;
          default:            
            break
        }
      }

      if (this._parsedParams) {
        for (let orIndex = 0; orIndex < this._parsedParams.where?.length; orIndex++) {
          const orElem = this._parsedParams.where[orIndex] 
          if (!firstOr) {
            where += ` OR `  
              
          }
         
          where += '('

          for (let andIndex = 0; andIndex < orElem.length; andIndex++) {
            const andElem = orElem[andIndex]
            where += `${andElem.field} ${cleanOperator(andElem.operator, andElem.notOperator)} ${andElem.value}`
            if (andIndex < orElem.length - 1) {
              where += ` AND `
            }
          }

          where += ')'
         
          firstOr = false
        }

        if (this._parsedParams.sort) {
          for (let index = 0; index < this._parsedParams.sort.length; index++) {
            const sortElem = this._parsedParams.sort[index]
            sort += `${sortElem.field}:${sortElem.order}`
            if (index < this._parsedParams.sort.length - 1) {
              sort += ' AND '
            }
          }
        }
      } 

      return {
        ...this._parsedParams, 
        where: where,
        sort: sort
      }     
    },
    throwErrorIfForbiddenField: (fieldName: string) => {      
      const fields = fieldName.split('.') 
      const field = fields[1] || fields[0]
      if (this._serviceOption.forbiddenFields.includes(field)) {
        throw new BadRequestException(`column ${fieldName} does not exist`)
      }
    }
  }

  public reqQueryParams = {
    parseAll:(query: iGetManyParams) => {

      const output: iParsedParams = {}
  
      if (!query.limit) {      
        query.limit = '100'
      } 
  
      if (!query.page) {
        query.page = '1'
      }

      if (!query.output) {
        query.output = eOuput.json
      }
  
      if (!this._reqQueryParams.isPopulated(query.sort)) {
        output.sort = this._reqQueryParams.parseSort(query.sort)
      }
  
      if (!this._reqQueryParams.isPopulated(query.filter)) {
        output.where = this._reqQueryParams.parseWhere(<string>query.filter)
      }
  
      if (!this._reqQueryParams.isPopulated(query.select)) {
        const select: string = query.select
        const selects = select.split(<string>this.options.VALUE_DELIMITER)
        if ( this._serviceOption && this._serviceOption.forbiddenFields) {
          
          for (let index = 0; index < selects.length; index++) {            
            const select = selects[index]
            this.helpers.throwErrorIfForbiddenField(select)           
          }
        }        
  
        output.select = []
        
        for (let index = 0; index < selects.length; index++) {         
          const element = selects[index].trim()
          if (!regExFieldNameOnly.test(element)) {
            throw new BadRequestException(`Can't be accepted as a select field: ${element}`)
            
          }
          output.select.push(element)
        }
      }
  
      // limit default is 100
      //todo: > env
      if (query.limit) {
        let limit = parseInt(query.limit.trim(), 10) 
        if (limit < 1) {
          limit = 100
        }
        output.limit = limit
      } else {
        output.limit = 100
      }
  
      if (query.page) {
        let page = (parseInt(query.page.trim(), 10) || 1) 
        if (page < 1) {
          page = 1
        }
        output.page = page
      } else {
        output.page = 1
      }

      if (query.extras) {
        output.extras = query.extras.split(',').map(i => i.trim())
      }

      if (query.view) {
        output.view = query.view.trim()
      }

      output.output = query.output || eOuput.json    
      
      this._parsedParams = output
  
      return output
    }
  }

  public queryBuilder = {
    addSelectFromParams:(queryBuilder: SelectQueryBuilder<CrudEntity>): SelectQueryBuilder<CrudEntity> => {
    
      // this.queryBuilder = queryBuilder
  
      queryBuilder = queryBuilder.select(this._parsedParams.select)
  
      return queryBuilder
    },
    addWhereFromParams:(queryBuilder: SelectQueryBuilder<CrudEntity>): SelectQueryBuilder<CrudEntity> => { 

      // this.queryBuilder = queryBuilder
  
      if (this._parsedParams && this._parsedParams.where && this._parsedParams.where.length > 0) {       
  
        // add and where first to keep an eventual where already set or to come
        queryBuilder = queryBuilder.andWhere(new Brackets(andWhereBb => { 
  
          for (let indexOr = 0; indexOr < this._parsedParams.where.length; indexOr++) {        
  
            const orClause = this._parsedParams.where[indexOr]        
  
            
              andWhereBb = andWhereBb.orWhere(new Brackets(qb => {          
                if (orClause && orClause.length > 0) {
                  for (let indexAnd = 0; indexAnd < orClause.length; indexAnd++) {
                    const andElement = orClause[indexAnd]              
                    this._queryBuilder.addWhereToQueryBuilder(andElement, qb)
                  }
                }  
              }))
            
            // queryBuilder = queryBuilder.orWhere(new Brackets(qb => {          
            //   if (orClause && orClause.length > 0) {
            //     for (let indexAnd = 0; indexAnd < orClause.length; indexAnd++) {
            //       const andElement = orClause[indexAnd]              
            //       this.addWhereToQueryBuilder(andElement, qb)
            //     }
            //   }  
            // }))
          }
  
        }))
        
      }
  
      return queryBuilder
    },    
    addPaginationFromParams:(select: SelectQueryBuilder<CrudEntity>) => {

      select = select
        .limit(this._parsedParams.limit)
        .offset((this._parsedParams.page -1 || 0) * this._parsedParams.limit)
  
      return select
    },
    addSortFromParams:(queryBuilder: SelectQueryBuilder<CrudEntity>): SelectQueryBuilder<CrudEntity> => {

      if (this._parsedParams.sort && this._parsedParams.sort.length > 0) {
        for (let index = 0; index < this._parsedParams.sort.length; index++) {
          const element = this._parsedParams.sort[index]
  
          queryBuilder = queryBuilder
            .addOrderBy(element.field, <any>element.order)    
        }
      }
  
      return queryBuilder
      
    },   
    getManyWithPagination: async ():Promise<iGetManyResult> => {

      let data: CrudEntity[]
      let countTotal: number

      switch (this._parsedParams.output) {
        case eOuput.raw:          
          data = await this.typeORMqueryBuilder.getRawMany()
          countTotal = await this.typeORMqueryBuilder.getCount()
          break        
        case eOuput.xlxs:          
          data = await this.typeORMqueryBuilder.getRawMany()
          countTotal = await this.typeORMqueryBuilder.getCount()
          
          const ws = XLSX.utils.json_to_sheet(data)
          const wb = XLSX.utils.book_new()
          XLSX.utils.book_append_sheet(wb, ws, 'data')
          
          /* generate buffer */
	        const buf = XLSX.write(wb, {type:'buffer', bookType:'xlsx', sheet: 'myName'})

          
          // const buf = XLSX.writeFile(wb, 'test.xlsx', {type: 'buffer'})
          return <any>new StreamableFile(buf)

          // /* generate an XLSX file */
          // XLSX.writeFile(wb, "sheetjs.xlsx")
          // console.log('sheetjs.xlsx written')
          break
        default:
          [data, countTotal] = await this.typeORMqueryBuilder     
            .getManyAndCount()
          break
      }

      let pageCount = Math.trunc(countTotal / this._parsedParams.limit)
      if ((countTotal % this._parsedParams.limit) != 0 ) { 
        pageCount++
      }
  
      let count = 0
      if (data && data.length) {
        count = data.length
      }

      const result: iGetManyResult = {
        count: count,
        page: this._parsedParams.page,
        pageCount: pageCount,
        total: countTotal,              
        data: data
      }

      if (this._parsedParams.extras) {
        result.extras = {}
        if (this._parsedParams.extras.includes(eExtras.query)) {
          result.extras.query = this.helpers.getHumanReadableParsedParams()           
        }
        // kind of hack.. 
        // created it to let know to the logger middleware, the metrics have to be populated
        if (this._parsedParams.extras.includes(eExtras.metrics)) {
          result.extras.metrics = {}
        }

        if (this._parsedParams.extras.includes(eExtras.help)) {
          result.extras.help = {
            comingSoon: 1
          }
        }
      }

      return result
    }  
  }
  
}

