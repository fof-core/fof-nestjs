import { ApiProperty } from "@nestjs/swagger"
import { SelectQueryBuilder } from "typeorm"
import { iFofPerf } from "../core/logger.middleware"
import { EntAuthUsers } from '../permissions/users/users.entity'

export enum eOuput {
  raw = 'raw',
  json = 'json',
  xlxs = 'xlxs',
  csv = 'csv'
}

export enum eExtras {
  metrics = 'metrics',
  query = 'query',
  help = 'help'
}

export class iGetManyParams {
  @ApiProperty()
  select: string
  @ApiProperty()
  filter: string
  @ApiProperty()
  sort: string 
  @ApiProperty()
  page: string
  @ApiProperty()
  limit: string  
  @ApiProperty()  
  view: string
  @ApiProperty()  
  extras: string
  @ApiProperty()  
  output: eOuput
  @ApiProperty()
  user: EntAuthUsers
}

export class iGetManyOptions {
  customQueryBuilder?: SelectQueryBuilder<any>
  fofPerf?: iFofPerf[]  
}

export class iGetManyResult {
  @ApiProperty({
    description: 'Number of records currently returned',
    example: `100`
  })
  count: number
  @ApiProperty({
    description: 'Total number of records founded',
    example: `1203`
  })  
  total: number
  @ApiProperty()
  @ApiProperty({
    description: 'Current page',
    example: `1`
  }) 
  page: number
  @ApiProperty({
    description: 'Total number of pages',
    example: `13`
  }) 
  pageCount: number
  @ApiProperty()
  data: any[]  
  @ApiProperty()
  extras?: any
}
