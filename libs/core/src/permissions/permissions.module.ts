import { Module, OnApplicationShutdown, OnModuleInit, Logger } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UsersService } from './users/users.service'
import { permissionsEntities } from './entities'
import { UsersController } from './users/users.controller'
import { OrganizationsService } from './organizations/organizations.service'
import { PermissionsService } from './permissions/permissions.service'
import { RolesService } from './roles/roles.service'
import { OrganizationsController } from './organizations/organizations.controller'
import { RolesController } from './roles/roles.controller'
import { PermissionsController } from './permissions/permissions.controller'
import { RolesPermissionsService } from './roles/rolesPermissions.service'
import { UsersRolesOrganizationService } from './roles/userRoleOrganization.service'
// import { OrganizationSubscriber } from './organizations/organization.entity.subscriber'

@Module({
  imports: [
    TypeOrmModule.forFeature([      
      ...permissionsEntities
    ])
  ],
  providers: [
    UsersService,
    OrganizationsService,
    PermissionsService,
    RolesService,
    RolesPermissionsService,
    UsersRolesOrganizationService,
    // OrganizationSubscriber
  ],
  exports: [
    UsersService,
    OrganizationsService,
    RolesService
  ],
  controllers: [
    UsersController,
    OrganizationsController,
    RolesController,
    PermissionsController
  ]
})
export class PermissionsModule implements OnApplicationShutdown, OnModuleInit {
  // private readonly logger = new Logger('FofCorePermissionsModule')  

  async onModuleInit () {    
    // this.logger.log('FofCorePermissionsModule started')
    // console.log(fofEnv())
  }

  async onApplicationShutdown() {
    //todo
  }
}
