import { Injectable } from '@nestjs/common'
import { EntAuthPermissions } from './permissions.entity'
import { crudServiceOptions, FofCrudService } from '../../crud/crud.service'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

@Injectable()
export class PermissionsService extends FofCrudService<EntAuthPermissions> {
  constructor(
    @InjectRepository(EntAuthPermissions) private repoPermissions: Repository<EntAuthPermissions>
  ) {
    super(repoPermissions)      
  }

  options: crudServiceOptions
}
