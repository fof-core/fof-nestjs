import { Controller, Get, UseGuards } from '@nestjs/common'
import { ApiSecurity, ApiTags } from '@nestjs/swagger'
import { PermissionsService } from './permissions.service'
import { fofAuthGuard } from '../../guard/auth.guard'
import { DtoPermission, EntAuthPermissions } from './permissions.entity'
import { eCrudMethod } from '../../crud/crud.service'
import { crudControllerFactory } from '../../crud/crud.controller.factory'
import { FofPerfLog } from '../../decorators/core.decorators'
import { iFofPerf, performanceHelper } from '../../core/logger.middleware'
import { GetManyParams } from '../../crud/crud.decorators'
import { iGetManyParams, iGetManyResult} from '../../crud/crud.interfaces'

@ApiTags('permissions')
@ApiSecurity('api-key')
@UseGuards(fofAuthGuard)
@Controller('permissions')
export class PermissionsController  
  extends crudControllerFactory<DtoPermission>({
    entityDto: DtoPermission,  
    entityMain: EntAuthPermissions,
    endpointsKeepOnly: [eCrudMethod.getMany]
  }) {
    
  constructor(
    public dtoService: PermissionsService
  ) {
    super(dtoService)
  }

  // @Get()
  // async getMany(@FofPerfLog() perfs: iFofPerf[], @GetManyParams() query: iGetManyParams):Promise<iGetManyResult> {
  //   const result =  await this.service.getMany(query, {fofPerf: perfs})
  //   return performanceHelper.decorateResult<iGetManyResult>(perfs, result)
  // }
}