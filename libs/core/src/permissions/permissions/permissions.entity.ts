import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn } from 'typeorm'
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger'
import { CrudEntity } from '../../crud/crud.entity'
import { EntAuthRolesPermissions } from '../roles/rolesPermissions.entitiy'

export class DtoPermission extends CrudEntity {
  static namePlural = 'permissions'
  static nameSingular = 'permission'

  @ApiProperty()
  @Column({ length: 100 })
  code: string

  @ApiProperty()
  @Column({ length: 100, nullable: true })
  description: string
}


@Entity('auth_permissions')
export class EntAuthPermissions extends DtoPermission { 

  @ApiHideProperty()
  // @JoinColumn({ name: 'permissionId' })
  @OneToMany(type => EntAuthRolesPermissions, rolesPermissions => rolesPermissions.permission, { cascade: true, onDelete: 'CASCADE' })
  joinRoles: EntAuthRolesPermissions[]

}