// exist !!!
// https://github.com/typeorm/typeorm/issues/2815

import { ViewEntity, ViewColumn, Connection, Index, Column } from 'typeorm'

@ViewEntity('auth_v_organizations_permissions',{ 
  expression: (connection: Connection) => connection.createQueryBuilder()
      .select('o.id', 'organizationParentId')      
      .addSelect('oflat.organizationChildId', 'organizationChildId')
      .addSelect('us.id', 'userId')
      .addSelect('p.code', 'permissionCode')
      .from('auth_users', 'us')
      .innerJoin('auth_users_roles_organizations', 'uro', 'us.id = uro.userId')
      .innerJoin('auth_roles', 'r', 'uro.roleId = r.id')
      .innerJoin('auth_roles_permissions', 'rp', 'r.id = rp.roleId')
      .innerJoin('auth_permissions', 'p', 'rp.permissionId = p.id')
      .innerJoin('auth_organizations', 'o', 'uro.organizationId = o.id')
      .innerJoin('auth_organizations_flat', 'oflat', 'o.id = oflat.organizationParentId')
      .groupBy('o.id')
      .addGroupBy('oflat.organizationChildId')
      .addGroupBy('us.id')
      .addGroupBy('p.code')      
})
export class vOrganizationsPermissions {  
  @ViewColumn()
  organizationParentId: string

  @ViewColumn()
  organizationChildId: string

  @ViewColumn()
  userId: string
  
  @ViewColumn()
  permissionCode: string  
}

