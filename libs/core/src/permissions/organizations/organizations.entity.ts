import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne,
  TreeChildren, TreeParent, Tree, Unique, PrimaryColumn, Index, JoinColumn } from 'typeorm'
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger'
import { CrudEntity } from '../../crud/crud.entity'
import { EntAuthUsersRolesOrganizations } from '../roles/userRoleOrganization.entity'
import { EntAuthUsers } from '../users/users.entity'
import { eCp } from '../permissions.enum'
import { IsNotEmpty, IsUUID, MaxLength } from 'class-validator'

export class organization extends CrudEntity {
  @ApiProperty({
    description: `Name of a department or a sector for exemple.<br>
    Depending on your structure, it could be a client or a partner as well`,
    example: 'IT department'  
  })
  @IsNotEmpty()
  @MaxLength(100)
  @Column({ length: 100})
  name: string

  @ApiProperty({
    description: `Free description`,
    example: `An awesome description for the IT departement`,
    required: false
  })
  // @MaxLength(200)
  @Column({ length: 200, nullable: true})
  description?: string

  @ApiProperty({
    description: `The parent's ID -> the internal one, NOT the UID`,
    example: `18`,
    // only the root can be empty    
    required: true
  })
  @ApiProperty()
  @IsUUID(4)
  @Column({nullable: true})
  parentId?: string
}

@Entity('auth_organizations')
@Tree('nested-set')
export class EntAuthOrganizations extends organization {
  static permissionCreate = eCp.organizationCreate
  static permissionUpdate = eCp.organizationUpdate
  static permissionDelete = eCp.organizationDelete
  static permissionRead = eCp.organizationRead
  static namePlural = 'organizations'
  static nameSingular = 'organization'

  @Column({default: false})
  isSynchronized?: boolean

  @TreeChildren()
  children?: EntAuthOrganizations[]

  @TreeParent() 
  parent?: EntAuthOrganizations

  @ApiHideProperty()
  @OneToMany(type => EntAuthUsersRolesOrganizations, usersRolesOrganizations => usersRolesOrganizations.role)
  usersRolesOrganizations?: EntAuthUsersRolesOrganizations[]

  @ApiHideProperty()
  @OneToMany(type => EntAuthUsers, users => users.organization)
  users?: EntAuthUsers[]

  @ApiHideProperty()
  @OneToMany(type => EntAuthOrganizationsFlat, organizationFlat => organizationFlat.organization, 
    { cascade: true,  onDelete: 'CASCADE' })
  @JoinColumn({ name: 'id' })
  organizationFlats?: EntAuthOrganizationsFlat[]
}

@Entity('auth_organizations_flat')
export class EntAuthOrganizationsFlat {  
  @PrimaryColumn({type: 'uuid'})  
  organizationParentId: string

  @PrimaryColumn({type: 'uuid'})
  @Index('auth_organizations_flat_idx_organizationChildId')
  organizationChildId: string

  @ManyToOne(type => EntAuthOrganizations, organization => organization.organizationFlats, 
    {onDelete: 'CASCADE' })
  @JoinColumn({ name: 'organizationChildId' })
  public organization?: EntAuthOrganizations
}