import { EventSubscriber, EntitySubscriberInterface, UpdateEvent, Connection, InsertEvent } from 'typeorm'
import { InjectConnection } from '@nestjs/typeorm'
// import { Organization } from '../organization.entity'
// import { CacheService } from '../../../shared/cache.service'
// import { FOF_DEFAULT_CONNECTION } from '../../../core/core.constants'
import { v4 as uuidv4 } from 'uuid'
import { EntAuthOrganizations } from './organizations.entity'

@EventSubscriber()
export class OrganizationSubscriber implements EntitySubscriberInterface<EntAuthOrganizations> {

    constructor (
			@InjectConnection() readonly connection: Connection,       
			// private cacheService: CacheService
    ) { 
      connection.subscribers.push(this)        
    }

    listenTo() {
      return EntAuthOrganizations
    }

    beforeInsert(event: InsertEvent<EntAuthOrganizations>) {			
      //ToDo: useless when update is made with a createQueryBuilder it seems...
      

      if (event.entity) {
        if (!event.entity._externalUID) {          
          event.entity._externalUID = uuidv4()
        }
      }

      console.log('event', event.entity)
    }
    
    // afterUpdate(event: UpdateEvent<Organization>) {			
    //   //ToDo: useless when update is made with a createQueryBuilder it seems...
    //   if (event.entity) {
    //     // console.log('update event')
    //     this.cacheService.del(`user_${event.entity.id}`)      
    //   }
    // }
}