import { Injectable, Logger, NotFoundException, UnauthorizedException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { getConnection, getManager, getRepository, Repository, UsingJoinColumnIsNotAllowedError } from 'typeorm'
import { EntAuthOrganizations, EntAuthOrganizationsFlat } from './organizations.entity'
import { EntAuthUsers } from '../users/users.entity'
import { vOrganizationsPermissions } from '../permissions.views'
import { eCp } from '../permissions.enum'
import { crudServiceOptions, ePermissionCheck, FofCrudService } from '../../crud/crud.service'
import { v4 as uuidv4 } from 'uuid'
import { iGetManyOptions, iGetManyParams, iGetManyResult } from '../../crud/crud.interfaces'
import { countReset } from 'console'

interface iFilteredTree extends EntAuthOrganizations {
  mustKeep?: boolean,
  mustKeepWhithChildren?: boolean,
  noAccess?: boolean
} 

@Injectable()
export class OrganizationsService extends FofCrudService<EntAuthOrganizations>{

  constructor(    
    @InjectRepository(EntAuthOrganizations) private repoOrganizations: Repository<EntAuthOrganizations>,
    @InjectRepository(vOrganizationsPermissions) private repoViewOrganizationsPermissions: Repository<vOrganizationsPermissions>
  ) {
    super(repoOrganizations)    
  }

  options: crudServiceOptions
  logger = new Logger('fofCore/organizations-service')

  async getOrganizationTree(user: EntAuthUsers):Promise<iFilteredTree[]> {
    // get the allowed organization for the user

    const userAllowedOrganizations:vOrganizationsPermissions[] = await this.repoViewOrganizationsPermissions
        .createQueryBuilder('vo')  
        .select('vo.organizationChildId')
        .where('vo.userId = :id', { id: user.id })
        .andWhere(`vo.permissionCode = '${eCp.organizationRead}'`)
        .getMany()  

    if (!userAllowedOrganizations) { throw new UnauthorizedException() }

    const newRoot:iFilteredTree = {
      id: "fake-root",
      name: 'All',
      children: []
    }

    const trees = await getManager().getTreeRepository(EntAuthOrganizations).findTrees()
    let treeIsRoot = true    

    const checkNode = (node: iFilteredTree, noAccess?: boolean) => { 
      let checkChildren = true
      // noAccess = true  
      // node.mustKeep = true
      // node.noAccess = noAccess       
      if (userAllowedOrganizations.filter(uao => uao.organizationChildId == node.id).length > 0) {
        // noAccess = false
        // node.mustKeep = true
        // node.noAccess = noAccess        
        checkChildren = false
        newRoot.children.push(node)
      }
      if (checkChildren && node.children) {
        for (let index = 0; index < node.children.length; index++) {
          treeIsRoot = false          
          checkNode(node.children[index], noAccess)
        }
      }
    }

    // check access right
    checkNode(trees[0])    

    if (treeIsRoot) {
      return trees
    }

    return [newRoot]
  }
  static async StaticOrganizationTreeFlat(organizationRepo: Repository<EntAuthOrganizations>) {
    const manager = organizationRepo.manager

    const tree = await manager.getTreeRepository(EntAuthOrganizations).findTrees()

    
    let flatTree: EntAuthOrganizationsFlat[] = []

    // iterate on the tree for flatening it with all deep children for each node
    const getSelectedNode = (node: EntAuthOrganizations, rootNode?: EntAuthOrganizations) => {
      if (flatTree.filter(m => m.organizationParentId === node.id && m.organizationChildId === node.id).length === 0) {
        flatTree.push({
          organizationParentId: node.id, 
          organizationChildId: node.id ,
          // organizationParentUid: node._externalUID,
          // organizationChildUid: node._externalUID
        })
      }
      if (rootNode && flatTree.filter(m => m.organizationParentId === rootNode.id && m.organizationChildId === node.id).length === 0) { 
        flatTree.push({
          organizationParentId: rootNode.id, 
          organizationChildId: node.id,
          // organizationParentUid: rootNode._externalUID,
          // organizationChildUid: node._externalUID
        }) 
      }
      node.children.forEach(child => {        
        if (rootNode)  { getSelectedNode(child, rootNode) }
        getSelectedNode(child, node)
      })
    }    
    
    if (tree && tree.length > 0) {
      getSelectedNode(tree[0])
    }

    // flatTree = flatTree.sort((a, b) => {
    //   if (a.organizationParentId > b.organizationParentId) return 1
    //   if (b.organizationParentId > a.organizationParentId) return -1
    // })

    return await getConnection().transaction(async transactionalEntityManager => {    
      
      await transactionalEntityManager
        .createQueryBuilder()
        .delete()
        .from(EntAuthOrganizationsFlat)        
        .execute()        
     
      return await transactionalEntityManager
        .createQueryBuilder()
        .insert()
        .into(EntAuthOrganizationsFlat)
        .values(flatTree)
        .execute()
    })
  }
  async OrganizationTreeFlatChange() {
    try {
      return await OrganizationsService.StaticOrganizationTreeFlat(this.repoOrganizations)  
    } catch (error) {
      throw error      
    }    
  }
  // override
  // organization is the only entiy protected by RBAC without an organizationId field..
  protected async throwUnauthorizedIfNotAllowed(user: EntAuthUsers, permissionToCheck: ePermissionCheck, dto?: EntAuthOrganizations): Promise<boolean> {
    let permissionCode: string
    let parentId: string

    permissionCode = this.entityType[permissionToCheck]
    parentId = dto.parentId

    // it means the entity doesn't have RBAC
    if (!permissionCode) { return true }

    // organizationId is mandatory for RBAC mecanism
    if (!parentId) { 
      throw new UnauthorizedException()       
    }

    const userAllowedOrganizations:vOrganizationsPermissions[] = 
        await getRepository(vOrganizationsPermissions)
          .createQueryBuilder('vo')  
          .select('vo.organizationChildId')
          .where('vo.userId = :id', { id: user.id })
          .andWhere(`vo.permissionCode = :permissionCode`, {permissionCode: permissionCode})    
          .andWhere('vo.organizationChildId =:parentId', {parentId: parentId})        
          .getMany() 
    
    if (userAllowedOrganizations.filter(op => op.organizationChildId === dto.parentId).length > 0) {
      return true
    }
    throw new UnauthorizedException()
  }

  public async createOne(dto: EntAuthOrganizations, user: EntAuthUsers):Promise<EntAuthOrganizations> {
    const parent = await this.repoOrganizations.findOne(dto.parentId)
    if (parent) {
      // bug with typeORM. Doesn't take parentId into account...
      dto.parent = parent
    }
    
    try {           
      const result = await super.createOne(dto, user) 
      this.OrganizationTreeFlatChange()  
      return result
    } catch (error) {
      throw error
    }
    
  }
  public async getMany(query: iGetManyParams, options?: iGetManyOptions):Promise<iGetManyResult> {
    // organization is the only entiy protected by RBAC without an organizationId field..
    let search = this.repo.createQueryBuilder('organization')
      // access management         
      .innerJoin('auth_v_organizations_permissions', 'op', `organization.id = op.organizationChildId`)
      .andWhere('op.userId = :userId', {userId: query.user.id})
      .andWhere(`op.permissionCode = '${EntAuthOrganizations.permissionRead}'`)
      
    return super.getMany(query, {customQueryBuilder: search})
  }
}

