import { Controller, Get, UseGuards } from '@nestjs/common'
import { ApiExcludeEndpoint, ApiOperation, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger'
import { EntAuthOrganizations, organization } from './organizations.entity'
import { OrganizationsService } from './organizations.service'
import { fofAuthGuard } from '../../guard/auth.guard'
import { EntAuthUsers } from '../users/users.entity'
import { FofUser } from '../../decorators/core.decorators'
import { crudControllerFactory } from '../../crud/crud.controller.factory'

@ApiTags('organizations')
@ApiSecurity('api-key')
@UseGuards(fofAuthGuard)
@Controller({
  path: 'organizations',
  version: '1'
})
export class OrganizationsController 
  extends crudControllerFactory<EntAuthOrganizations>({
    entityDto: EntAuthOrganizations,
    entityMain: EntAuthOrganizations  
  }) {
    
  constructor (
    public dtoService: OrganizationsService
  ) {
    super(dtoService)
  } 
  
  @ApiOperation({
    summary:'Get all organizations in once', 
    description: `Get all organizations in once, ordered in a treeview structure, according to the current user data access`
  })  
  @ApiResponse({ status: 200, description: 'Organization treeview for the current user', type: EntAuthOrganizations})
  @Get('getTreeView')
  async getTreeview(@FofUser() user: EntAuthUsers): Promise<EntAuthOrganizations[]> {      
    this.dtoService.OrganizationTreeFlatChange()
    const result = await this.dtoService.getOrganizationTree(user)
    // console.log('treeview result', result)
    return result
  }

  // @ApiResponse({ status: 201, description: 'Create one organization', type: organization})
  // @Override()
  // async createOne (
  //   @ParsedRequest() req: CrudRequest,
  //   @ParsedBody() dto: EntAuthOrganizations,
  // ) {     
  //   const result = await this.base.createOneBase(req, dto)
  //   this.service.OrganizationTreeFlatChange()
  //   return result
  // }

}
