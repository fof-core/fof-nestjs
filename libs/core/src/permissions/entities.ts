import { EntAuthUsers } from './users/users.entity'
import { EntAuthOrganizations, EntAuthOrganizationsFlat } from './organizations/organizations.entity'
import { EntAuthPermissions } from './permissions/permissions.entity'
import { EntAuthRoles } from './roles/roles.entity'
import { EntAuthUsersRolesOrganizations } from './roles/userRoleOrganization.entity'
import { EntAuthRolesPermissions } from './roles/rolesPermissions.entitiy'
import { EntAuthUsersLogins } from './users/usersLogin.entity'
import { vOrganizationsPermissions } from './permissions.views'

export const permissionsEntities = [
  EntAuthUsers,
  EntAuthOrganizations, EntAuthOrganizationsFlat,
  EntAuthPermissions, EntAuthRoles, EntAuthUsersRolesOrganizations,
  EntAuthRolesPermissions,
  vOrganizationsPermissions
]

