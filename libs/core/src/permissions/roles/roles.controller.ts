import { Body, Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common'
import { ApiProperty, ApiSecurity, ApiTags } from '@nestjs/swagger'
import { RolesService, eView } from './roles.service'
import { EntAuthRoles } from './roles.entity'
import { CreateBulkRelationMethod, CreateOneRelationMethod, 
  DeleteOneRelationMethod, GetManyParams, GetManyRelationMethod } from '../../crud/crud.decorators'
import { fofAuthGuard } from '../../guard/auth.guard'
import { iGetManyParams } from '../../crud/crud.interfaces'
import { DtoRole } from './roles.entity'
import { DtoPermission, EntAuthPermissions } from '../permissions/permissions.entity'
import { EntAuthUsersRolesOrganizations } from './userRoleOrganization.entity'
import { crudControllerFactory } from '../../crud/crud.controller.factory'
import { EntAuthUsers } from '../users/users.entity'
import { FofUser } from '../../decorators/core.decorators'


class dtoRolePermissionCreate {  
  @ApiProperty()   
  public permissionId!: string
}

@ApiTags('roles')
@ApiSecurity('api-key')
@UseGuards(fofAuthGuard)
@Controller({
  path: 'roles',
  version: '1'
})
export class RolesController 
  extends crudControllerFactory<EntAuthRoles>({
    entityDto: DtoRole,  
    entityMain: EntAuthRoles,  
    views: eView
  }) {

  constructor(
    private dtoService: RolesService    
  ) {
    super(dtoService)    
  }

  @GetManyRelationMethod({
    entityDto: DtoPermission,
    entityMain: EntAuthPermissions,
    summary: `Get many permissions from that role`,
    description: `Get many permissions linked to that role.<br>
    `
  })
  @Get(':roleId/permissions')    
  async getManyPermissions(@Param('roleId') roleId: string, @GetManyParams() query: iGetManyParams) {        
    return await this.dtoService.permissions.getMany(roleId, query)            
  }

  @CreateOneRelationMethod({
    entityDto: DtoPermission,
    entityMain: EntAuthPermissions,
    summary: `Link a permission`,
    description: `Create a relation between that role and a given permission.`
  })
  @Post(':roleId/permissions')
  async createOnePermission(@Param('roleId') roleId: string, @Body() dto: dtoRolePermissionCreate, @FofUser() user: EntAuthUsers) {
    return await this.dtoService.permissions.createOne(roleId, <any>dto, user)
  }

  @DeleteOneRelationMethod({    
    entityDto: DtoPermission,
    entityMain: EntAuthPermissions,
    summary: `Delete a link with a permissions`,
    description: `Delete a relation between that role and a given permission.`
  })
  @Delete(':roleId/permissions/:permissionId')
  async deleteOnePermission(@Param('roleId') roleId: string, @Param('permissionId') permissionId: string):Promise<any> {    
    return await this.dtoService.permissions.deleteOne(roleId, permissionId)
  }

  @CreateBulkRelationMethod({
    entityDto: EntAuthUsersRolesOrganizations,
    entityMain: EntAuthUsersRolesOrganizations,
    summary: `Link a role, a user and an organization`,
    description: `Create a relation between a role, a user and an organization.<br>
    It's a ternary relationship`
  })
  @Post('users/organizations/bulk')
  async createManyUsersOrganization(       
    @Body() dto: EntAuthUsersRolesOrganizations[]
  ) {
    return await this.dtoService.userOrganization.createMany(dto)
  }
 
}

