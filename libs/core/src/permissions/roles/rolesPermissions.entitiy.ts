import { Entity, Column, PrimaryColumn, ManyToOne, Index, JoinColumn } from 'typeorm'
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger'
import { CrudEntityWithoutId, CrudEntity } from '../../crud/crud.entity'
import { EntAuthRoles } from './roles.entity'

import { EntAuthPermissions } from '../permissions/permissions.entity'
// import { Permission } from './permission.entity'
import { eCp } from '../permissions.enum'
import { IsUUID } from 'class-validator'

@Entity('auth_roles_permissions')
// @Index('auth_roles_permissions_uq_idx', ['roleId', 'permissionId'], { unique: true })
export class EntAuthRolesPermissions extends CrudEntityWithoutId {
  static permissionCreate = eCp.roleCreate  
  static permissionUpdate = eCp.roleUpdate  
  static permissionDelete = eCp.roleDelete
  static permissionRead = eCp.roleRead

  @ApiProperty()  
  @PrimaryColumn()
  @IsUUID(4)
  public permissionId!: string

  @ApiProperty()  
  @PrimaryColumn()
  @IsUUID(4)
  public roleId!: string

  @ApiProperty()
  @Column({nullable: true})
  public order: number

  // @ApiProperty()
  @ApiHideProperty()
  // @JoinColumn({ name: 'permissionId' })
  @ManyToOne(type => EntAuthPermissions, permission => permission.joinRoles)
  public permission!: EntAuthPermissions

  @ApiHideProperty()
  // @JoinColumn({ name: 'roleId' })
  @ManyToOne(type => EntAuthRoles, role => role.joinPermissions, { onDelete:'CASCADE' })
  public role!: EntAuthRoles
}