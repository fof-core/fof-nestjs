import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository, SelectQueryBuilder } from 'typeorm'
import { EntAuthRoles } from './roles.entity'
import { crudServiceOptions, FofCrudService } from '../../crud/crud.service'
// import { BaseService } from '../../crud/crud.repo'
import { PermissionsService } from '../permissions/permissions.service'
import { EntAuthUsersRolesOrganizations } from './userRoleOrganization.entity'
import { targetModulesByContainer } from '@nestjs/core/router/router-module'


@Injectable()
export class UsersRolesOrganizationService extends FofCrudService<EntAuthUsersRolesOrganizations> {
  constructor(
    @InjectRepository(EntAuthUsersRolesOrganizations) public repoUsersRolesOrganization: Repository<EntAuthUsersRolesOrganizations>,     
   ) {
    super(repoUsersRolesOrganization) 
   }

   options: crudServiceOptions
}