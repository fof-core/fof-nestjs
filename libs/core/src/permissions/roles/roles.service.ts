import { Injectable, MethodNotAllowedException, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository, SelectQueryBuilder } from 'typeorm'
import { EntAuthRoles } from './roles.entity'
import { crudServiceOptions, FofCrudService } from '../../crud/crud.service'
import { PermissionsService } from '../permissions/permissions.service'
import { iGetManyParams } from '../../crud/crud.interfaces'
import { EntAuthPermissions } from '../permissions/permissions.entity'
import { RolesPermissionsService } from './rolesPermissions.service'
import { EntAuthRolesPermissions } from './rolesPermissions.entitiy'
import { UsersRolesOrganizationService } from './userRoleOrganization.service'
import { EntAuthUsersRolesOrganizations } from './userRoleOrganization.entity'
import { EntAuthUsers } from '../users/users.entity'

export enum eView {
  rolePermissions = 'rolePermissions'
}

@Injectable()
export class RolesService extends FofCrudService<EntAuthRoles> {

  constructor(
    @InjectRepository(EntAuthRoles) private repoRoles: Repository<EntAuthRoles>,
    @InjectRepository(EntAuthPermissions) private repoPermissions: Repository<EntAuthPermissions>,
    private rolesPermissionsService: RolesPermissionsService,
    private permissionsService :PermissionsService,
    private usersRolesOrganizationService: UsersRolesOrganizationService
  ) {
    super(repoRoles) 

    this.disableRBACOrganizationCheck = true

    this.addView({
      name: eView.rolePermissions,      
      query:() => { 
        return this.repoRoles.createQueryBuilder('roles') 
          // .leftJoinAndMapMany('permissions', 'role.joinPermissions', 'joinPermissions')
          .leftJoinAndSelect('roles.joinPermissions', 'joinPermissions')
          .leftJoinAndSelect('joinPermissions.permission', 'permission')
          // .leftJoinAndMapMany('permissions', 'joinPermissions.permission', 'permission')        
      }
    })

    // const columns = []

    // const cols = getConnection().getMetadata(this.entityType).ownColumns.map(column => {
    //   return {
    //     isPrimary: column.isPrimary,        
    //     type: typeof column.type != 'string' ? typeof (column.type as any)() : column.type,
    //     name: column.propertyPath,
    //     length: column.length,
    //     precision: column.precision 
    //   }
    // })
    
    // console.log(`${this.entityType.name} cols:`, cols)
 
  }

  options: crudServiceOptions

  userOrganization = {
    createMany: async (dto: EntAuthUsersRolesOrganizations[]) => {
      if (!dto) {
        throw new MethodNotAllowedException(`body can't be null`)        
      }      
     
      const result = this.usersRolesOrganizationService.
        repoUsersRolesOrganization.createQueryBuilder()
          .insert()
          .values(dto)
          .execute()
      
      if (result) {
        return true
      }
      return false
    }
  }

  permissions = {
    getMany: async (roleId: string,  query: iGetManyParams) => {
      let search =  this.repoPermissions.createQueryBuilder('permissions')
        .innerJoin('permissions.joinRoles', 'joinRoles')        
        .where('joinRoles.roleId = :roleId', {roleId: roleId})
       
      return await this.permissionsService.getMany(query, {customQueryBuilder: search})     
    },
    createOne: async (roleId: string, dto: EntAuthRolesPermissions, user: EntAuthUsers) => {
      if (!dto) {
        throw new MethodNotAllowedException(`body can't be null`)        
      }
      if (!dto.permissionId) {
        throw new MethodNotAllowedException(`permissionId can't be null`)        
      }
      dto.roleId = roleId
      return await this.rolesPermissionsService.createOne(dto, user)
    },    
    deleteOne: async (roleId: string, permissionId: string) => { 

      const found = await this.rolesPermissionsService.repoRolesPermissions.findOne({
        permissionId: permissionId,
        roleId: roleId
      })

      if (!found) {
        throw new NotFoundException()
      }
      
      return this.rolesPermissionsService.repoRolesPermissions.delete({
        permissionId: permissionId,
        roleId: roleId
      })
    }
  }
  
}
