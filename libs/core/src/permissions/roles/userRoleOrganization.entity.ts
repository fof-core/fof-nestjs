import { Entity, Column, PrimaryColumn, ManyToOne, Unique } from 'typeorm'
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger'
import { CrudEntityWithoutId, CrudEntity } from '../../crud/crud.entity'

import { EntAuthRoles } from './roles.entity'
import { EntAuthUsers } from '../users/users.entity'
import { EntAuthOrganizations } from '../organizations/organizations.entity'

import { eCp } from '../permissions.enum'
import { IsUUID } from 'class-validator'

@Entity('auth_users_roles_organizations')
@Unique('auth_users_roles_organizations_uq_idx', ['userId', 'roleId', 'organizationId'])
export class EntAuthUsersRolesOrganizations extends CrudEntityWithoutId {
  static permissionCreate = eCp.userRoleCreate
  static permissionUpdate = eCp.userRoleUpdate
  static permissionDelete = eCp.userRoleDelete
  static permissionRead = eCp.userRoleRead

  @ApiProperty()  
  @PrimaryColumn()
  @IsUUID(4)
  public userId!: string

  @ApiProperty()  
  @PrimaryColumn()
  @IsUUID(4)
  public roleId!: string

  @ApiProperty()  
  @PrimaryColumn()
  @IsUUID(4)
  public organizationId!: string

  @ApiProperty()
  @Column({nullable: true})
  public order: number
  
  @ApiHideProperty()
  @ManyToOne(type => EntAuthUsers, user => user.usersRolesOrganizations, { onDelete: 'CASCADE' })
  public user!: EntAuthUsers

  // @ApiProperty({type:() => DtoRole})
  @ApiHideProperty()
  @ManyToOne(type => EntAuthRoles, role => role.usersRolesOrganizations)
  public role!: EntAuthRoles

  // @ApiProperty({type:() => Organization})
  @ApiHideProperty()
  @ManyToOne(type => EntAuthOrganizations, organization => organization.usersRolesOrganizations)
  public organization!: EntAuthOrganizations
}