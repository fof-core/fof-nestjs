import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository, SelectQueryBuilder } from 'typeorm'
import { EntAuthRoles } from './roles.entity'
import { crudServiceOptions, FofCrudService } from '../../crud/crud.service'
// import { BaseService } from '../../crud/crud.repo'
import { PermissionsService } from '../permissions/permissions.service'
import { EntAuthRolesPermissions } from './rolesPermissions.entitiy'
import { targetModulesByContainer } from '@nestjs/core/router/router-module'

@Injectable()
export class RolesPermissionsService extends FofCrudService<EntAuthRolesPermissions> {
  constructor(
    @InjectRepository(EntAuthRolesPermissions) public repoRolesPermissions: Repository<EntAuthRolesPermissions>,     
   ) {
    super(repoRolesPermissions) 
   }

   options: crudServiceOptions
}