import { Entity, Column, OneToMany } from 'typeorm'
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger'
import { CrudEntity } from '../../crud/crud.entity'
import { EntAuthUsersRolesOrganizations } from './userRoleOrganization.entity'
import { EntAuthRolesPermissions } from './rolesPermissions.entitiy'
import { eCp } from '../permissions.enum'
import { IsNotEmpty, Matches, MaxLength } from 'class-validator'
import { Type } from 'class-transformer'
import { Crud } from '../../crud/crud.decorators'

@Crud({summary: 'testOnly'})
export class DtoRole extends CrudEntity {

  @ApiProperty({
    description: `Code of the role.<br>
    !! That code must be unique and without any special charactere, included space, except '-' and '_'<br>
    e.g. invoices_manage, invoices-manage, invoicesManage, ...`,
    example: 'invoices_manager'    
  })
  @Matches(/^[A-Za-z-_]+$/, {message: `code can only contains alpha, '-' and '_' without space`})
  @IsNotEmpty()  
  @Column({ length: 100, nullable: false, unique: true })
  code: string

  @ApiProperty({
    description: 'Free description of the role.',
    example: `Users with that role can create, update and delete an invoice until the closing day of the financial year.`,
    // nullable: true,
    required: false
  })  
  @MaxLength(255)
  @Column({ length: 255, nullable: true})  
  description?: string

  permissions?: EntAuthRolesPermissions
}

@Entity('auth_roles')
export class EntAuthRoles extends DtoRole {
  static permissionCreate = eCp.roleCreate
  static permissionUpdate = eCp.roleUpdate
  static permissionDelete = eCp.roleDelete
  static permissionRead = eCp.roleRead
  static namePlural = 'roles'
  static nameSingular = 'role'

  @ApiHideProperty()
  @OneToMany(type => EntAuthUsersRolesOrganizations, usersRolesOrganizations => usersRolesOrganizations.role)
  usersRolesOrganizations?: EntAuthUsersRolesOrganizations[]

  @ApiHideProperty()
  @OneToMany(type => EntAuthRolesPermissions, rolesPermissions => rolesPermissions.role, { cascade: true, onDelete:'CASCADE' })
  joinPermissions: EntAuthRolesPermissions[]
}