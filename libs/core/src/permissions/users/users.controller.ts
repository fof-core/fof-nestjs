import { Body, Controller, Delete, Get, HttpStatus, Param, Put, Query, Req, UseGuards } from '@nestjs/common'
import { ApiBody, ApiOperation, ApiQuery, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger'
// import { Crud, CrudAuth, CrudController } from '@nestjsx/crud'
import { EntAuthUsers } from './users.entity'
import { fofAuthGuard } from '../../guard/auth.guard'
import { UsersService } from './users.service'
import { crudControllerFactory } from '../../crud/crud.controller.factory'
import { FofUser } from '../../decorators/core.decorators'

@ApiTags('users')
@ApiSecurity('api-key')
@UseGuards(fofAuthGuard)
@Controller({
  path: 'users',
  version: '1'
})
export class UsersController 
    extends crudControllerFactory<EntAuthUsers>({
      entityDto: EntAuthUsers,
      entityMain: EntAuthUsers 
    }) {

  constructor(
    public dtoService: UsersService
  ) {
    super(dtoService)    
  }

  @ApiOperation({
    summary:'Current user', 
    description: `Return the authenticated user`
  })
  @ApiResponse({ status: 200, description: 'Current authentified user', type: EntAuthUsers})
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: `Unauthorized` })
  @Get('current')  
  async getCurrentUser(@Req() req: any): Promise<EntAuthUsers> {        
    return req.user
  }

  @Get(':id/roles')
  async getOneWithRole(    
    @Param('id') id: number,
    @FofUser() user: EntAuthUsers,
    ): Promise<EntAuthUsers> {  
    return await this.dtoService.userGetOneWithRoleById(id, user)    
  }

  @ApiOperation({
    summary:'Replace user roles for one organization', 
    description: `Replace all the user roles for a given organization by the sent roles, 
                  even if they are not in the same organisation`
  }) 
  // @ApiBody({ type: [UserRoleOrganization] })       
  @Put(':userId/organization/:organisationId/roles')
  async userRolesUpdate(
      @FofUser() user: EntAuthUsers,
      @Param('userId') userId: number,  
      @Param('organisationId') organisationId: string,        
      @Body() userRoleOrganizations: any[]
  ): Promise<any>  {     
    return await this.dtoService.roleByUserAndOrganizationSave(userId, organisationId, userRoleOrganizations, user)    
  }

  @ApiOperation({
    summary:'Delete user roles for several organizations', 
    description: `Array of organizationId: All user roles relative to that organization will be deleted`
  })
  @ApiQuery({ name: 'ids', description: 'Must be an array in json format' })
  @ApiBody({ type: [Number] })     
  @Delete(':userId/organizations')
  async userRolesDelete(      
      @FofUser() user: EntAuthUsers,
      @Param('userId') userId: string,   
      @Query('ids') organizationsId: string      
  ): Promise<any> {      
    const orgsId: string[] = JSON.parse(organizationsId)    
    return await this.dtoService.roleByUserAndOrganizationsDelete(userId, orgsId, user)          
  }

}
