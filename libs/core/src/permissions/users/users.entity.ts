import { ApiHideProperty, ApiProperty } from "@nestjs/swagger"
import { Entity, Column, OneToMany, ManyToOne, Index } from 'typeorm'
import { CrudEntity } from '../../crud/crud.entity'
import { EntAuthOrganizations } from '../organizations/organizations.entity'
import { EntAuthUsersRolesOrganizations } from '../roles/userRoleOrganization.entity'
// import { EntAuthUsersLogins } from './usersLogin.entity'
import { eCp } from '../permissions.enum'
import { IsBoolean, IsNotEmpty, IsUUID, MaxLength } from "class-validator"
import { Type } from "class-transformer"

// export interface iFofUser { 
//   authUserId?: string,  
//   authUserFullName?: string 
// }

@Entity('auth_users')
export class EntAuthUsers extends CrudEntity { 
  static permissionCreate = eCp.userCreate  
  static permissionUpdate = eCp.userUpdate  
  static permissionDelete = eCp.userDelete
  static permissionRead = eCp.userRead
  static namePlural = 'users'
  static nameSingular = 'user'

  @ApiProperty({ 
    description: `Active or unactive the user`, 
    example: false,
    required: false 
  })  
  @IsBoolean()
  @Column({ default: true })
  isActive?: boolean

  @ApiProperty({ 
    description: `First Name of the user` ,
    example: 'Albert'
  }) 
  @IsNotEmpty()  
  @MaxLength(100)
  @Column({length: 100})
  firstName?: string

  @ApiProperty({ 
    description: `Last Name of the user`,
    example: 'Camus'
  }) 
  @IsNotEmpty()  
  @MaxLength(100)
  @Column({ length: 100 })
  lastName?: string

  @ApiProperty({ 
    description: `eMail address of the user`,
    example: 'ac@cafeparis.com'
  }) 
  @IsNotEmpty()
  @MaxLength(100)
  @Column({length: 100})
  eMail?: string

  @ApiHideProperty()
  @Column({nullable: true, select: false})
  apiToken?: string

  @ApiProperty({ 
    description: `the branch Id of the organization`,
    example: '48f0bdeb-b7ec-45b7-a7bd-d27f49199a51'
  })
  @IsUUID(4)
  @Column({ nullable: true })
  organizationId?: string
  
  @ApiHideProperty()
  @Type(() => EntAuthOrganizations)
  @ManyToOne(type => EntAuthOrganizations, organizations => organizations.users)
  public organization?: EntAuthOrganizations

  @ApiHideProperty()
  @Type(() => EntAuthUsersRolesOrganizations)
  @OneToMany(() => EntAuthUsersRolesOrganizations, usersRolesOrganizations => usersRolesOrganizations.user, { 
    cascade: true,  onDelete: 'CASCADE' })
  public usersRolesOrganizations?: EntAuthUsersRolesOrganizations[]

  permissions?: string[]
  // @ApiHideProperty()
  // @OneToMany(() => EntAuthUsersLogins, usersLogins => usersLogins.user)
  // public usersLogins?: EntAuthUsersLogins[]
}