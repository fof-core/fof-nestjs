import { Injectable, UnauthorizedException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { getConnection, Repository, SelectQueryBuilder } from 'typeorm'
import { EntAuthUsers } from './users.entity'
import { crudServiceOptions, FofCrudService } from '../../crud/crud.service'
import { iGetManyOptions, iGetManyParams } from '../../crud/crud.interfaces'
import { vOrganizationsPermissions } from '../permissions.views'
import { EntAuthUsersRolesOrganizations } from '../roles/userRoleOrganization.entity'

export interface iUserAuth {
  // id: number,
  apiToken?: string, 
  eMail?: string
}

@Injectable()
export class UsersService extends FofCrudService<EntAuthUsers> {

  options: crudServiceOptions = {
    forbiddenFields: ['apiToken']
  }

  constructor(
    @InjectRepository(EntAuthUsers) repoUser: Repository<EntAuthUsers>,
    @InjectRepository(vOrganizationsPermissions) private repoAccess: Repository<vOrganizationsPermissions>
  ) {
    super(repoUser)
      
    // this.addView({
    //   name: 'userWithCompany',      
    //   query:() => { 
    //     return this.repo.createQueryBuilder(this.nameSingular)        
    //       .leftJoinAndSelect('role.permissions', 'rp')
    //       .leftJoinAndSelect('rp.permission', 'permission')
    //   }
    // })
  }

  async getUserAndAuthorizationsBySearch(query: iUserAuth): Promise<any> {

    if (!query || (!query.apiToken && !query.eMail)) {
      throw new Error(`getUserAndAuthorizationsById: no user to search`)      
    }

    let search = this.repo
      .createQueryBuilder('user') 
      .leftJoinAndSelect('user.usersRolesOrganizations', 'usersRolesOrganizations')
      .leftJoinAndSelect('usersRolesOrganizations.role', 'role')
      .leftJoinAndSelect('role.joinPermissions', 'joinPermissions')
      .leftJoinAndSelect('joinPermissions.permission', 'permission')
      .where('user.isActive = true')
    
    if (query.apiToken) {
      search = search
        .andWhere('user.apiToken = :apiToken', { apiToken: query.apiToken })
    }

    if (query.eMail) {
      search = search
        .andWhere('user.eMail = :eMail', { eMail: query.eMail })
    }

    let  result: EntAuthUsers

    try {       
      result = await search.getOne()      
    } catch (error) {      
      throw error       
    }
    

    if (!result) {
      throw new UnauthorizedException('user_not_authorized_to_log_into_app')
    }

    // flat user roles permissions before caching him    
    const userPermissions = []

    if (result.usersRolesOrganizations) {
      result.usersRolesOrganizations.forEach(userRoleOrganization => {
        userRoleOrganization.role.joinPermissions.forEach(rolePermission => {            
          if (userPermissions.filter(up => up === rolePermission.permission.code).length === 0)  {
            userPermissions.push(rolePermission.permission.code)
          }            
        })
      })
    }

    //toDo: real dto for user auth and caching
    delete result['usersRolesOrganizations']
    delete result['apiToken']
    
    result['permissions'] = userPermissions

    return result
  }

  public async getMany(query: iGetManyParams, options?: iGetManyOptions):Promise<any> {

    let search = this.repo.createQueryBuilder(this.namePlural)
      // access management  
      .innerJoinAndSelect('users.organization', 'organization')
      .innerJoin('auth_v_organizations_permissions', 'op', 'users.organizationId = op.organizationChildId')      
      .andWhere('op.userId = :userId', {userId: query.user.id})
      .andWhere("op.permissionCode = 'userRead'")  

    return super.getMany(query, {...options, customQueryBuilder: search})
  }
  
  async userGetOneWithRoleById(id: number, user: EntAuthUsers): Promise<EntAuthUsers> {   
    // console.log(this.repo.metadata.targetName)
    // await this.operationIsAllowed(user.id, ePermissionCheck.update, req)

    // impossible to generate a IN subquery in a join with typeorm    
    const allowedOrgs = await this.repoAccess
      .createQueryBuilder('op')
        .select('op.organizationChildId')
        .where('op.userId = :userId', {userId: user.id})
        .andWhere("op.permissionCode = 'organizationRead'")
        .getMany()


    const allowedOrgsArray = allowedOrgs.map(org => org.organizationChildId)
    const quotedAndCommaSeparated = "'" + allowedOrgsArray.join("','") + "'"

    // const foundedUser = await getRepository(EntAuthUsers, FOF_DEFAULT_CONNECTION)
    const foundedUser = this.repo
      .createQueryBuilder('user') 
      // access management  
      .innerJoin('auth_v_organizations_permissions', 'op', 'user.organizationId = op.organizationChildId')  
      .leftJoinAndSelect('user.usersRolesOrganizations', 'usersRolesOrganizations', 
        `usersRolesOrganizations.organizationId IN (${quotedAndCommaSeparated})`)      
      .leftJoinAndSelect('usersRolesOrganizations.organization', 'organization')
      .leftJoinAndSelect('usersRolesOrganizations.role', 'role')
      .where('user.id = :id', { id: id }) 
      // access management
      .andWhere('op.userId = :userId', {userId: user.id})
      .andWhere("op.permissionCode = 'userRead'")    
      .orderBy('organization.name', 'DESC')

    return await foundedUser.getOne()
  }

  async roleByUserAndOrganizationSave(userId: number, organizationId: string, 
      userRoleOrganizations: any[], user: EntAuthUsers) {
    return await getConnection().transaction(async transactionalEntityManager => {    

      // this.cacheService.del(`user_${userId}`) 

      const allowedOrgs = await this.repoAccess
        .createQueryBuilder('op')
          .select('op.organizationChildId')
          .where('op.userId = :userId', {userId: user.id})
          .andWhere("op.permissionCode = 'organizationRead'")
          .getMany()

      if (allowedOrgs.filter(op => op.organizationChildId === organizationId).length < 1) {
        throw new UnauthorizedException('user is not authorized to manage this organization') 
      }

      userRoleOrganizations.forEach(org => {
        if (allowedOrgs.filter(op => op.organizationChildId === org.organizationId).length < 1) {
          throw new UnauthorizedException('user is not authorized to manage this organization') 
        }
      })

      await transactionalEntityManager
        .createQueryBuilder()
        .delete()
        .from(EntAuthUsersRolesOrganizations)
        .where('userId = :id', { id: userId })
        .andWhere('organizationId = :organizationId', { organizationId: organizationId })
        .execute()        
     
      return await transactionalEntityManager
        .createQueryBuilder()
        .insert()
        .into(EntAuthUsersRolesOrganizations)
        .values(userRoleOrganizations)
        .execute()
    })
  }

  async roleByUserAndOrganizationsDelete(userId: string, organizationsId: string[], user: EntAuthUsers) {    
    // this.cacheService.del(`user_${userId}`) 

    const allowedOrgs = await this.repoAccess
      .createQueryBuilder('op')
        .select('op.organizationChildId')
        .where('op.userId = :userId', {userId: user.id})
        .andWhere("op.permissionCode = 'organizationRead'")
        .getMany()

    organizationsId.forEach(id => {
      if (allowedOrgs.filter(op => op.organizationChildId === id).length < 1) {
        throw new UnauthorizedException('user is not authorized to manage this organization') 
      }
    })   

    // console.log('organizationsId', organizationsId)
    
    return await this.repoAccess
      .createQueryBuilder()
      .delete()
      .from(EntAuthUsersRolesOrganizations)
      .where('userId = :id', { id: userId })
      .andWhere('organizationId IN (:...organizationsId)', { organizationsId: organizationsId })
      .execute()
    
    // return await getManager(FOF_DEFAULT_CONNECTION)
    //   .createQueryBuilder()
    //   .delete()
    //   .from(UserRoleOrganization)
    //   .where('userId = :id', { id: userId })
    //   .andWhere('organizationId IN (:...organizationsId)', { organizationsId: organizationsId })
    //   .execute()
  }
}
  
