import { Entity, Column, PrimaryGeneratedColumn, OneToMany, CreateDateColumn, ManyToOne, JoinColumn } from 'typeorm'
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger'
import { EntAuthUsers } from './users.entity'

@Entity('auth_users_logins')
export class EntAuthUsersLogins {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  id?: string

  @ApiProperty()
  @CreateDateColumn()
  date?: Date
  
  @ApiProperty()
  @Column({ length: 255, nullable: true})
  description: string

  @ApiProperty()
  @Column({ nullable: true })
  userId: string

  @ApiProperty()
  @Column({ nullable: true })
  userOtherId: string
  
  // @ApiHideProperty()
  // @ManyToOne(type => EntAuthUsers, user => user.usersLogins, { nullable: true })
  // public user: EntAuthUsers
}