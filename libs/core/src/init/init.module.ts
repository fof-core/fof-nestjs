import { Module } from '@nestjs/common'
import { InitService } from './init.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { permissionsEntities } from '../permissions/entities'
import { PermissionsModule } from '../permissions/permissions.module'

@Module({
  imports: [    
    PermissionsModule,
    TypeOrmModule.forFeature([      
      ...permissionsEntities
    ])
  ],
  providers: [
    InitService
  ]
})
export class InitModule {}
