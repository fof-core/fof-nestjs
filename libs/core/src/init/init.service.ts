import { Inject, Injectable, Logger } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { EntAuthUsers } from '../permissions/users/users.entity'
import { EntAuthRoles } from '../permissions/roles/roles.entity'
import { UsersService } from '../permissions/users/users.service'
import { v4 as uuid } from 'uuid'
import { EntAuthPermissions } from '../permissions/permissions/permissions.entity'
import { EntAuthRolesPermissions  } from '../permissions/roles/rolesPermissions.entitiy'
import { eCp } from '../permissions/permissions.enum'
import { FofCoreModuleOptions, FOF_CORE_OPTIONS } from '../core.interface'
import { EntAuthOrganizations } from '../permissions/organizations/organizations.entity'
import { EntAuthUsersRolesOrganizations } from '../permissions/roles/userRoleOrganization.entity'
import { OrganizationsService } from '../permissions/organizations/organizations.service'
import * as path from 'path'
import * as XLSX from 'xlsx'

@Injectable()
export class InitService {
  constructor(    
    @Inject(FOF_CORE_OPTIONS) private config: FofCoreModuleOptions,
    @InjectRepository(EntAuthUsers) private repoUsers: Repository<EntAuthUsers>,
    @InjectRepository(EntAuthPermissions) private readonly repoPermissions: Repository<EntAuthPermissions>,
    @InjectRepository(EntAuthRoles) private readonly repoRoles: Repository<EntAuthRoles>,
    @InjectRepository(EntAuthRolesPermissions) private readonly repoRolesPermissions: Repository<EntAuthRolesPermissions>,
    @InjectRepository(EntAuthOrganizations) private readonly repoOrganizations: Repository<EntAuthOrganizations>,
    @InjectRepository(EntAuthUsersRolesOrganizations) private readonly repoUsersRolesOrganizations: Repository<EntAuthUsersRolesOrganizations>,
    private organizationsService: OrganizationsService
  ) {       
    this.init()
  }

  private readonly logger = new Logger('FofCore-init')

  async init() {
    this.initFileRead()
    this.defaultDataAdd()

    await this.organizationsEnsure()
    await this.permissionsEnsure()
    await this.usersEnsure()
    await this.rolesEnsure()
  }

  private initFileRead() {

    const filePath = path.resolve(process.cwd(), 'data/dataInit.xlsx')
    
    let workbook: XLSX.WorkBook

    try {      
      workbook = XLSX.readFile(filePath)
      this.logger.log('found the init/dataInit.xlsx init data file')
      
      workbook.SheetNames.forEach(sheetName => {
        const sheet = workbook.Sheets[sheetName]
        const jsonSheet = XLSX.utils.sheet_to_json(sheet)
        
        switch (sheetName) {
          case 'users':            
            this.defaultUsers = jsonSheet            
            break        
          case 'roles':            
            this.defaultRoles = jsonSheet
            break          
          case 'organizations':            
            this.defaultOrganizations = jsonSheet            
            break
          case 'usersRoles':            
            this.defaultUsersRoles = jsonSheet                        
            break
          case 'rolesPermissions':
            this.defaultRolesPermissions = jsonSheet                                    
          default:
            break
        } 
      })

    } catch (error) {
      this.logger.log(`didn't found the init/dataInit.xlsx init data file`)
    }
  }

  private defaultUsers: any[] = []
  private defaultRoles: any[] = []
  private defaultOrganizations: any[] = []
  private defaultUsersRoles: any[] = []
  private defaultRolesPermissions: any[] = []
  private permissionsAll: string[] = []

  
  defaultDataAdd() {
    // super user
    this.defaultUsers.unshift(
      {uid:'superuser', eMail: 'superuser@fof.com', 
        firstName: 'Super', lastName: 'User', organizationUid: 'root', isActive: true,
        apiToken: uuid()} 
    )

    // default organization
    this.defaultOrganizations.unshift({_externalUID: 'root', name:'root' })

    // roles
    this.defaultRoles.unshift({code: 'superUser'})
    this.defaultRoles.unshift({code: 'userManager'})
    this.defaultRoles.unshift({code: 'organizationManager'})
    this.defaultRoles.unshift({code: 'roleManager'})
  }

  async usersEnsure() {
     // add default users   
     for (let index = 0; index < this.defaultUsers.length; index++) {
      const user = this.defaultUsers[index]

      if (user.isActive == 'true' || user.isActive == '1') {
        user.isActive = true
      }
      
      let wUser: EntAuthUsers = await this.repoUsers.findOne({where: {eMail: user.eMail}})      
      if (!wUser) {        
        wUser = new EntAuthUsers()
        this.repoUsers.merge(wUser, user)
        
        if (user.organizationUid) {          
          const organization: EntAuthOrganizations = await this.repoOrganizations.findOne({where: {_externalUID: user.organizationUid}})                    
          // wUser.organization = organization          
          wUser.organizationId = organization.id
        }     

        // console.log('wUser', wUser)

        wUser = await this.repoUsers.save(wUser)   
      }
    }

    this.logger.log(`DB - Users checked`)
  }

  async permissionsEnsure() {
    if (this.config && this.config.permissions) {
      const permissions = { ...this.config.permissions, ...eCp }

      const permissionArray: string[] = Object.values(permissions)
      this.permissionsAll = permissionArray 
      for (let index = 0; index < permissionArray.length; index++) {
        const permission = permissionArray[index]
        let wPermission: EntAuthPermissions = await this.repoPermissions.findOne({where: {code: permission}})
        if (!wPermission) {        
          wPermission = new EntAuthPermissions()
          wPermission.code = permission               
          wPermission = await this.repoPermissions.save(wPermission)
        }  
      }
    }
    this.logger.log(`DB - Permissions checked`)
  }

  async rolesEnsure() {
    // Add default roles
    for (let index = 0; index < this.defaultRoles.length; index++) {
      const role = this.defaultRoles[index]
      let wRole: EntAuthRoles = await this.repoRoles.findOne({where: {code: role.code}})
      if (!wRole) {        
        wRole = new EntAuthRoles()
        this.repoRoles.merge(wRole, role)          
        wRole = await this.repoRoles.save(wRole)        
      }
    }

    // add permission to role function 
    const addPermisionToRole = async (permission: string, role: string) => {
      let wPermission: EntAuthPermissions = await this.repoPermissions.findOne({where: {code: permission}})      
      let wRole: EntAuthRoles = await this.repoRoles.findOne({where: {code: role}})     
      let wRolePermission: EntAuthRolesPermissions = await this.repoRolesPermissions.findOne({where: {permissionId: wPermission.id, roleId: wRole.id}})
      if (!wRolePermission) {
        wRolePermission = new EntAuthRolesPermissions()
        wRolePermission.permission = wPermission
        wRolePermission.role = wRole
        wRolePermission = await this.repoRolesPermissions.save(wRolePermission)
      }
    }

    const permissions = this.permissionsAll
    
    // superUser -> all permissions
    for (let index = 0; index < permissions.length; index++) {
      const permission = permissions[index]
      await addPermisionToRole(permission, 'superUser')
    }

    // roleManager
    await addPermisionToRole(eCp.roleCreate, 'roleManager')
    await addPermisionToRole(eCp.roleDelete, 'roleManager')
    await addPermisionToRole(eCp.roleRead, 'roleManager')
    await addPermisionToRole(eCp.roleUpdate, 'roleManager')

    // userManager
    await addPermisionToRole(eCp.userCreate, 'userManager')
    await addPermisionToRole(eCp.userDelete, 'userManager')
    await addPermisionToRole(eCp.userRead, 'userManager')
    await addPermisionToRole(eCp.userUpdate, 'userManager')  
    await addPermisionToRole(eCp.userRoleCreate, 'userManager')
    await addPermisionToRole(eCp.userRoleDelete, 'userManager')
    await addPermisionToRole(eCp.userRoleUpdate, 'userManager')
    await addPermisionToRole(eCp.userRoleRead, 'userManager')

    //add role to user
    const addRolesToUser = async (role: string, userEmail: string, orgizationCode?: string) => {
      let wRole: EntAuthRoles = await this.repoRoles.findOne({where: {code: role}})
      let wUser: EntAuthUsers = await this.repoUsers.findOne({where: {eMail: userEmail}})
      let wOrganization: EntAuthOrganizations
      if (orgizationCode) {
        wOrganization = await this.repoOrganizations.findOne({where: {_externalUID: orgizationCode}})
      }
      let wUserRoleOrganization: any = await this.repoUsersRolesOrganizations.findOne({where: {roleId: wRole.id, userId: wUser.id}})
      if (!wUserRoleOrganization) {
        wUserRoleOrganization = new EntAuthOrganizations()
        wUserRoleOrganization.role = wRole
        wUserRoleOrganization.user = wUser
        if (orgizationCode) {
          wUserRoleOrganization.organization = wOrganization
        }
        wUserRoleOrganization = await this.repoUsersRolesOrganizations.save(wUserRoleOrganization)
      }      
    }

    // add roles to users
    await addRolesToUser('superUser','superuser@fof.com', 'root')
    
    for (let index = 0; index < this.defaultUsersRoles.length; index++) {
      const userRole = this.defaultUsersRoles[index]      
      await addRolesToUser(userRole.roleCode, userRole.userEmail , userRole.OrganizationUid)
    }  

    this.logger.log(`DB - roles checked`)
  }
  
  async organizationsEnsure() {    

    let organizationAdded = false
    
    for (let index = 0; index < this.defaultOrganizations.length; index++) {     
      const orgItem = this.defaultOrganizations[index]

      let wOrganization:EntAuthOrganizations = await this.repoOrganizations.findOne({where: {_externalUID: orgItem._externalUID}})
    
      if (!wOrganization) {
        const orgToSave: EntAuthOrganizations = new EntAuthOrganizations()      
        this.repoOrganizations.merge(orgToSave, orgItem)  
        if (!orgItem.parentUid && orgItem._externalUID !== 'root') {
          orgItem.parentUid = 'root'
        }

        if (orgItem.parentUid) {
          let wOrganizationParent:EntAuthOrganizations = await this.repoOrganizations.findOne({where: {_externalUID: orgItem.parentUid}})          
          // orgToSave.parentId = wOrganizationParent.id
          orgToSave.parent = wOrganizationParent
        }

        try {         
          await this.repoOrganizations.save(orgToSave)    
        } catch (error) {
          console.log('to: manage error', error)
        }
        
        organizationAdded = true
      }       
      
    }

    if (organizationAdded) {     
      await this.organizationsService.OrganizationTreeFlatChange()     
    }  

    this.logger.log(`DB - Organizations checked`)
  }
}
