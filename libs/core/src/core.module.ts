import { DynamicModule, Module } from '@nestjs/common'
import { FofCoreSingletonModule } from './core-singleton.module'
import { FofCoreModuleOptions } from './core.interface'
import { PermissionsModule } from './permissions/permissions.module'
import { AllExceptionsFilter } from './core/all-exceptions.filter'
import { APP_FILTER } from '@nestjs/core'

@Module({
  imports: [
    PermissionsModule    
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter
    }
  ],
  exports: [
    PermissionsModule
  ]
})
export class FofCoreModule {
  static forRoot(coreOptions: FofCoreModuleOptions): DynamicModule {
    return {
      module: FofCoreModule,
      imports: [
        FofCoreSingletonModule.forRoot(coreOptions)
      ],
      exports: [
        
      ]     
    }
  }

  static forFeature() {
    // tbc
  }
}
