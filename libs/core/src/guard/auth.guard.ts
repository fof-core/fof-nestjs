import { Injectable, Logger } from "@nestjs/common"
import { AuthGuard } from "@nestjs/passport"
// import { GlobalExchange } from '../config/global'

// allow Multiple Strategies
// https://github.com/nestjs/nest/issues/2116

const logger = new Logger('FofCore')

const authGuardResolve = () => {
  if (process.env.AUTH_GUARDS) {
    return process.env.AUTH_GUARDS.split(' ')
  }  
  // console.log('alors?', GlobalExchange.authModules.all())
  logger.error('AUTH_GUARDS must be set in .env file -> generate AuthGuard error')  
}

@Injectable()
export class fofAuthGuard extends AuthGuard(authGuardResolve()) {}