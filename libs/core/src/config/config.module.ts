import { Module } from '@nestjs/common'
// Get config vars from env or .env files
// https://docs.nestjs.com/techniques/configuration
import { ConfigModule as NestConfigModule } from '@nestjs/config'
import { fofEnv } from './configuration'

@Module({
  imports: [
    NestConfigModule.forRoot({
      // factory for config, simple helper for completion
      load: [fofEnv],
      //allow to use the config in all modules without registering it
      isGlobal: true
    }),
  ]
})
export class ConfigModule {}
