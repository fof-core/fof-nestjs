/** Internal use only */
global.globalExchange = {
  authModules: []
}
export abstract class GlobalExchange { 
  static authModules = {
    add:(moduleName: string) => {
      global.globalExchange.authModules.push(moduleName)
    },
    all:() => {
      return global.globalExchange.authModules
    }
  }
}