/** To enable intellisense capability on env file*/
export interface efofConfig { 
  debug: {
    error500: boolean,
    performance: boolean
  }  
}

/** 
 * Transform env vars into typed config
 * Doc here: https://docs.nestjs.com/techniques/configuration
 */
export const fofEnv = ():efofConfig => ({  
  debug: {
    error500: process.env.DEBUG_ERROR_500 == 'true',
    performance: process.env.DEBUG_PERFORMANCE == 'true'
  }  
})



