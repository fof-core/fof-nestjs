import { INestApplication, VersioningType, ValidationPipe } from '@nestjs/common'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
// https://github.com/iamolegga/nestjs-pino change encore
// import { Logger } from "nestjs-pino"
import * as swStats from 'swagger-stats'
import { performance } from 'perf_hooks'
import * as fs from 'fs'
import { join } from 'path'
import { performanceLogger } from './core/logger.middleware'
import { fofEnv } from './config/configuration'

declare const module: any;

export class iFofAppConfigOptions {
  /** add a simulated latency in ms */
  simulateLatencyInMs?: number = 1000
  /** add the  swaggerstat module. 
   * @see https://swaggerstats.io/ 
  */
  swStatsActivate?: boolean = false
  /**
   * Set an alternative logger.
   * DO NOT USE for now.
   */
  // abstract pinoLoggerActive?: boolean = false
  /**
   * Set cors for DEV PURPOSE ONLY!
   */
  corsDev?: boolean = false
  performanceLog? : boolean = false
}

// sleep time expects milliseconds
function sleep (time = 1000) {
  return new Promise((resolve) => setTimeout(resolve, time))
}

function readMem() {
  const mem = process.memoryUsage()  
  const convert = { Kb: n => (n / 1024), Mb: n => convert.Kb(n) / 1024 }
  const toHuman = (n, t) => `${convert[t](n).toFixed(2)}${t}`
  return `Used ${toHuman(mem.heapUsed, 'Mb')} of ${toHuman(mem.heapTotal, 'Mb')} - RSS: ${toHuman(mem.rss, 'Mb')}`
}

const startTime = performance.now()
let _serviceTechnicalName: string = 'fofCore'

export const fofBoostrapStats = async (app: INestApplication) => {
  console.info(`┌────────────────────────────────────────────────────────────┐`);
  console.info(`| Service ${_serviceTechnicalName} successfully started`) 
  console.info(`| Listen on port ${await app.getUrl()}`) 
  console.info(`| Bootstrap done in ${(performance.now() - startTime).toFixed(3)}ms`);
  console.info(`| Memory: ${readMem()}`) 
  console.info(`└────────────────────────────────────────────────────────────┘`);
}

export const fofAppConfig = async (app: INestApplication, fofAppOptions?: iFofAppConfigOptions) => {

  // const serviceTechnicalName: string = app.get('ConfigService').get('service.technicalName')
  // const serviceName: string = app.get('ConfigService').get('service.name')
  // const rabbitMQUrl = app.get('ConfigService').get('rabbitMQ.url')

  // _serviceTechnicalName = serviceTechnicalName
  
  // app.useGlobalPipes(
  //   new ValidationPipe({
  //     whitelist: true,
  //     transform: true,      
  //     disableErrorMessages: false,
  //   })
  // )
  
  const env = fofEnv()

  if (env.debug?.performance) {
    app.use(performanceLogger)
  }  
  
  if (module.hot) {    
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }


  if (fofAppOptions) {

    // if (fofAppOptions.pinoLoggerActive) {
    //   app.useLogger(app.get(Logger))
    // }

    if (fofAppOptions.simulateLatencyInMs) {
      // simulate latency
      app.use(async (req: any, res: any, next: any) => {
        await sleep(fofAppOptions.simulateLatencyInMs)
        next()
      })
    }

    if (fofAppOptions.corsDev) {
      const corsOptions = {    
        origin: ($origin, cb) => {
    
          // accept all origins
          // toDo: accept clientWhiteList only: wait for API gw to do the job
          cb(null, true)
         
          // if (['http://my.url.in.space'].indexOf($origin) > -1) {
          //   cb(null, true);
          // }  else {
          //   cb(new Error('Not allowed by CORS'));
          // }
        },
        methods: "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS",
        // allowedHeaders: ['Content-Type', 'Authorization'],
        credentials: false,
        preflightContinue: false,
        optionsSuccessStatus: 200
      }
        
      app.enableCors(corsOptions)
    }

    // if (fofAppOptions.performanceLog) {

    // }

  }

  // app.setGlobalPrefix('v1.0', {
  //   // exclude: [{ path: 'health', method: RequestMethod.GET }],
  //   exclude: []
  // });
  
  const options = new DocumentBuilder()
    // .addBearerAuth()
    .addApiKey({type: 'apiKey', name: 'X-API-KEY', in: 'header'}, 'api-key')
    // .setTitle(`${serviceName} APIs`)
    .setDescription(`Documentation for user and access management API`)
    // .setDescription(`Documentation for ${serviceName} API`)
    
    // .setBasePath('/v1.0')    
    .setVersion('v1.0')    
    // .addTag('organizations', `Only accessible to the users with a super admin power (lvl 60 required)`)
    // .addTag('Auth/Basic')
    // .addTag('Auth/Kerberos')
    .build()    
  const document = SwaggerModule.createDocument(app, options, {
    include: []
  })

  const filePath =  join(__dirname, '..', 'public')
  const cert = fs.writeFileSync(`${filePath}\\openApi.json`, JSON.stringify(document))

  // console.log('doc', document)

  // SwaggerModule.setup('docs/iam', app, document)
  SwaggerModule.setup('docs/apis', app, document)
  
  // http://localhost:3000/swagger-stats/ui
  // https://swaggerstats.io/  
  // if (fofAppOptions && fofAppOptions.swStatsActivate) {
    // app.use(swStats.getMiddleware({
    //   name: 'fofCore',
    //   swaggerSpec:document
    // }))
  // }

  // app.enableVersioning({
  //   type: VersioningType.HEADER,
  //   header: 'api-version'
  //   // prefix: 'v'
  //   // key: 'v='
  // })
}