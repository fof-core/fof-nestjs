import { createParamDecorator, ExecutionContext, Get, Query, Type, UseGuards } from '@nestjs/common'
import { EntAuthUsers } from '../permissions/users/users.entity'
import { applyDecorators } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiOperation, ApiProperty, ApiQuery, ApiResponse } from '@nestjs/swagger'
import { getConnection } from 'typeorm'
import { EntAuthRoles, DtoRole } from '../permissions/roles/roles.entity'
import { performanceHelper } from '../core/logger.middleware'

export const FofUser = createParamDecorator(
  //toDo: don't get an ExecutionContext in debbug mode?
  (data: unknown, ctx: any):EntAuthUsers => {
    try {
      const request = ctx.switchToHttp().getRequest()
      return request.user
    } catch (error) {
      return ctx.user
    }
  }
)

export const FofPerfLog = createParamDecorator(
  //toDo: don't get an ExecutionContext in debbug mode?
  (data: string, ctx: any):EntAuthUsers => {
    try {
      const request = ctx.switchToHttp().getRequest()     
      performanceHelper.tick(request.fofPerf, data || 'fofCore authorization')
      return request.fofPerf
    } catch (error) {
      return ctx.user
    }
  }
)



