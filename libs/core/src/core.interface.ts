import { DynamicModule, ForwardReference, Type } from "@nestjs/common"

export interface FofCoreModuleOptions {  
   /** a string enum for your action RBAC used in roles */
   permissions?: any,
   entities?: any[]
}

/** for internal use only */
export const FOF_CORE_OPTIONS = 'FOF_CORE_OPTIONS'