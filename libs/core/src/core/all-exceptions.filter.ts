import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus, Logger } from '@nestjs/common'
import { ApiProperty } from '@nestjs/swagger'
import { QueryFailedError } from 'typeorm'
import { EntAuthUsers } from '../permissions/users/users.entity'
// import { logger } from './logger.service'

export enum eErrorType {
  validationError = 'ValidationError',
  invalidUID = 'InvalidUID',
  other = 'Other'
}

export class ErrorResponse {
  @ApiProperty({example: 400}) 
  statusCode: number
  @ApiProperty({example: 'malformed entity'})
  message: string
  @ApiProperty({example: 'code can only contains alpha'})
  error: string
  @ApiProperty({example: '{detailledError: ...}'})
  detail: object
}

export class DtoError {
  // @ApiProperty({type: ErrorResponse})
  // response: ErrorResponse  
  @ApiProperty({example: 400})
  status: string
  @ApiProperty({example: 'malformed entity'})
  message: string
  @ApiProperty({example: 'BadRequestException'})
  name: string
  @ApiProperty()
  response: any
}


@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost): Error {
    const ctx = host.switchToHttp()
    const response = ctx.getResponse()
    const request = ctx.getRequest()
    let errorIsManagedByFofCore = false
    let errorNeedToBeTransformed = true
    let logError = true

    let status: HttpStatus = HttpStatus.NOT_ACCEPTABLE 
    let responseError: any
    
    const user:EntAuthUsers = request.user

    // general http error
    if (!errorIsManagedByFofCore && exception instanceof HttpException) { 
      status = exception.getStatus()
      errorIsManagedByFofCore = true

      if ([HttpStatus.UNAUTHORIZED].includes(status)) {
        errorNeedToBeTransformed = false
        logError = false
      }

      if ([HttpStatus.BAD_REQUEST, HttpStatus.NOT_FOUND].includes(status)) {
        errorNeedToBeTransformed = false
      }

    }

    // typeorm error
    if (!errorIsManagedByFofCore && exception instanceof QueryFailedError) {       
      errorIsManagedByFofCore = true
      status = HttpStatus.BAD_REQUEST
      responseError = {
        response: {
          statusCode: status,
          message: exception.message,
          error: exception['detail'],
          detail: exception
        },
        status: status,
        message: 'Bad Request Exception',
        name: 'BadRequestException'
      }
    }   
    
    if (!errorIsManagedByFofCore && exception instanceof TypeError) { 
      errorIsManagedByFofCore = true
      status = HttpStatus.I_AM_A_TEAPOT
      // delete exception.stack
      errorNeedToBeTransformed = true
      responseError = {
        response: {
          statusCode: status,
          message: exception.message,
          error: exception.name,
          detail: exception.stack
        },
        status: status,
        message: exception.message,
        name: exception.name
      }
    }

    if (!errorIsManagedByFofCore && exception instanceof Error) { 
      errorIsManagedByFofCore = true
      status = HttpStatus.I_AM_A_TEAPOT
      // delete exception.stack
      errorNeedToBeTransformed = true
      responseError = {
        response: {
          statusCode: status,
          message: exception.message,
          error: exception.name,
          detail: exception.stack
        },
        status: status,
        message: exception.message,
        name: exception.name
      }
    }

    if (logError) {
      console.log('fof error', exception)
      console.log('type', typeof exception)
    }
    
    if (!errorNeedToBeTransformed) {
      response.status(status).json(exception)
      return
    } 

    if (errorIsManagedByFofCore) {
      response.status(status).json(responseError)
      return
    }

    console.log('fof AllExceptionsFilter error: not managed error ---------------------')
   
    // console.log('fof error type', typeof exception)
    response.status(status).json({
      response: {
        statusCode: status,
        message: exception.message,
        error: exception.name,
        detail: exception.stack
      },
      status: status,
      message: exception.message,
      name: exception.name
    })    
  }
}

/*

standard message
 
{
    "response": {
        "statusCode": 400,
        "message": [
            "only alpha, '-' and '_' are allowed"
        ],
        "error": "Bad Request"
    },
    "status": 400,
    "message": "Bad Request Exception",
    "name": "BadRequestException"
}

 */