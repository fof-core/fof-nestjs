import { Request, Response, NextFunction } from 'express'
import { performance } from 'perf_hooks'
import { fofEnv} from '../config/configuration'

export interface iFofPerf {
  time: number,
  message: string,
  offset?: number | string
}

const globalEnv = fofEnv()

export const performanceHelper = {
  tick:(perf: iFofPerf[] | Request, message: string) => {
    if (!perf) { return }
    if (!globalEnv.debug?.performance) { return }
    if ( perf['fofPerf']) { perf = perf['fofPerf'] }    

    perf.push({
      time: performance.now(),
      message: message
    })
  },
  getSummmary:(perfs: iFofPerf[]) => {    
    const steps: string[] = []

    let offset = 0
    let total = 0
    for (let index = 0; index < perfs.length; index++) {
      const element = perfs[index]      
      if (index > 0) {
        offset = Math.round(element.time - perfs[index - 1].time)
        total = total + offset
      } 
      element.offset = offset  
      if (index == 0) {
        steps.push(`Calling ${element.message}`)    
      } else {
        steps.push(`${element.message} +${element.offset}ms`)    
      }
      
    }    
    return {
      total: `${total}ms`,
      steps: steps
    }
  },
  decorateResult:<T>(perfs: iFofPerf[], result: any):T => {
    if (!result.extras?.metrics) { return result }
    performanceHelper.tick(perfs, 'post processing')
    const perfResult = performanceHelper.getSummmary(perfs)

    result.extras.metrics = perfResult
    
    return result
  }
}

export function performanceLogger(req: any, res: Response, next: NextFunction) {
  if (!globalEnv.debug?.performance) { return }
  req.fofPerf = []
  performanceHelper.tick(req.fofPerf, `${req.originalUrl}`)
  next()
}
