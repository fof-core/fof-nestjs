import { DynamicModule, Global, Module, Logger, OnApplicationShutdown, OnModuleInit } from '@nestjs/common'
import { ConfigModule } from './config/config.module'
import { FofCoreModuleOptions, FOF_CORE_OPTIONS } from './core.interface'
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm'
import { getConnectionOptions } from 'typeorm'
import { permissionsEntities } from './permissions/entities'
import { InitModule } from './init/init.module'
import { ServeStaticModule } from '@nestjs/serve-static'
import { join } from 'path'

const publicPath =  join(__dirname, '..', 'public')
console.log('publicPath', publicPath)


@Global()
@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: publicPath,
      renderPath: '/public*'
      // serveStaticOptions: {}
    }),
    ConfigModule,
    InitModule    
  ]
})
export class FofCoreSingletonModule implements OnApplicationShutdown, OnModuleInit {
  private readonly logger = new Logger('FofCoreModule')  

  async onModuleInit () {    
    this.logger.log('FofCoreModule is starting')
    // console.log(fofEnv())
  }

  static forRoot(coreOptions: FofCoreModuleOptions): DynamicModule {

    const customEntities = coreOptions.entities || []
   
    const fofCoreModuleOptions = {
      provide: FOF_CORE_OPTIONS,
      useValue: coreOptions,
    }

    return {
      module: FofCoreSingletonModule,
      imports: [   
        //todo parameter            
        TypeOrmModule.forRootAsync({   
          // get the entry named 'default' in ormconfig.yml and add the entities from the project
          name: 'default',     
          useFactory: async (): Promise<TypeOrmModuleOptions> => {  
            // const options = await getConnectionOptions('fof-default')  
            let options: TypeOrmModuleOptions
            try {             
              options = await getConnectionOptions('default')              
            } catch (error) {
              // logger.log('Typeorm: no configuration file founded')   
              console.log('error typeorm cnx', error)           
            }                
            const typeOrmOptions = {
              ...options,          
              entities: [
                ...permissionsEntities,
                ...customEntities
              ]
            }
            return typeOrmOptions             
          }
        }),   
      ],
      providers: [
        fofCoreModuleOptions
      ],
      exports: [
        fofCoreModuleOptions
      ]    
    }
  }
  
  async onApplicationShutdown() {
    //todo
  }
}
