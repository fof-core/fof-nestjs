import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne,
  TreeChildren, TreeParent, Tree, Unique, PrimaryColumn, Index, JoinColumn } from 'typeorm'
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger'

import { CrudEntity, EntAuthUsers, EntAuthOrganizations } from '@fof-nestjs/core'
import { eP } from '../permissions.enum'
import { IsNotEmpty, MaxLength, MinLength } from 'class-validator'
import { Exclude, Expose } from 'class-transformer'

@Entity('auth_organizations')
export class EntAuthOrganizationsCustom extends EntAuthOrganizations {
  // @ApiProperty({
  //   description: `other_property_for_test.<br>`,
  //   example: 'other_property_for_test'  
  // })
  // @Column({ length: 100, nullable: true})
  // other_property_for_test: string

  @ApiHideProperty()
  @OneToMany(type => EntCompany, company => company.organization)
  public companies: EntCompany[]
}


@Entity('companies')
export class EntCompany extends CrudEntity {
  static namePlural = 'companies'
  static nameSingular = 'company'

  @ApiProperty({
    description: `Name of the company.<br>`,
    example: 'Coca-Cola'  
  })
  @IsNotEmpty() 
  @MaxLength(100)
  @Column({ length: 100 })
  @Expose()
  name: string

  @ApiHideProperty()
  @ManyToOne(type => EntAuthOrganizationsCustom, organizations => organizations.companies)
  public organization?: EntAuthOrganizationsCustom  
}

// @Entity('companies_organizations')
// export class EntCompanyOrganization extends EntityCore {
//   @ApiProperty()
//   @Column()
//   company: string

//   @ApiProperty()
//   @Column()
//   organization: string
// }

