import { Module } from '@nestjs/common'
import { CompaniesService } from './companies.service'
import { CompaniesController } from './companies.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { companiesEntities } from './entities'

@Module({
  imports: [
    TypeOrmModule.forFeature([      
      ...companiesEntities
    ])
  ],
  providers: [
    CompaniesService
  ],
  controllers: [
    CompaniesController
  ]
})
export class CompaniesModule {}
