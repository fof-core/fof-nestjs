import { crudControllerFactory, fofAuthGuard } from '@fof-nestjs/core'
import { Body, ClassSerializerInterceptor, Controller, Param, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common'
import { ApiSecurity, ApiTags } from '@nestjs/swagger'
import { CompaniesService } from './companies.service'
import { EntCompany } from './companies.entity'

// https://stackoverflow.com/questions/63244163/nestjs-do-i-need-dtos-along-with-entities

@ApiTags('companies')
@ApiSecurity('api-key')
@UseGuards(fofAuthGuard)
@Controller({
  path: 'companies',
  version: '1'
})
@Controller('companies')
export class CompaniesController 
    extends crudControllerFactory<EntCompany>({
      entityDto: EntCompany,
      entityMain: EntCompany      
    }) {

  constructor(
    private dtoService: CompaniesService
  ) {
    super(dtoService)    
  }
}
