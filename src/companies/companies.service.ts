import { FofCrudService, crudServiceOptions } from '@fof-nestjs/core'
import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { EntCompany } from './companies.entity'

@Injectable()
export class CompaniesService  extends FofCrudService<EntCompany>{
  constructor(
    @InjectRepository(EntCompany) repo: Repository<EntCompany>
  ) {
    super(repo)      
  }

  options: crudServiceOptions
}
