export enum eP {  
  invoiceCreate = 'invoiceCreate',
  invoiceUpdate = 'invoiceUpdate', 
  invoiceRead = 'invoiceRead',
  invoiceDelete = 'invoiceDelete',
  invoiceExportExcel = 'invoiceExportExcel',
  clientExportPDF = 'clientExportPDF'
}