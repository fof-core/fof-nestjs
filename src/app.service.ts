import { Injectable } from '@nestjs/common'
import { RolesService } from '@fof-nestjs/core'

@Injectable()
export class AppService {

  constructor( 
    private rolesService: RolesService 
  ) {
    // this.rolesService.notifUpdataOne$.subscribe(result => {
    //   console.log('rolesService.notifUpdataOne$', result)
    // })
  }

  getHello(): string {
    return 'Hello World!'
  }
}
