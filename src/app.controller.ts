import { Controller, Get, Req, UseGuards } from '@nestjs/common'
import { AppService } from './app.service'
// import { appAuthGuard } from './app.config'
import { fofAuthGuard } from '@fof-nestjs/core'
import * as fs from 'fs'
import { join } from 'path'
import { ApiOperation } from '@nestjs/swagger'

// @UseGuards(fofAuthGuard)
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  
  @UseGuards(fofAuthGuard)
  @Get()
  getHello(@Req() req): string {
    console.log('user', req.user)
    return this.appService.getHello()
  }

  @Get('openAPI')
  getOpenAPI() {
    const filePath =  join(__dirname, '..', 'public')
    const spec = fs.readFileSync(`${filePath}\\openApi.json`, 'utf-8')
    return JSON.parse(spec)
  }

  // @ApiOperation({
  //   summary:'Current user', 
  //   description: `Override the authenticated user`
  // })
  // @Get('users/current')  
  // async getCurrentUser(@Req() req: any): Promise<any> {        
  //   return req.user
  // }
}
