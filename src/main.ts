import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { fofAppConfig, fofBoostrapStats } from '@fof-nestjs/core'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  await fofAppConfig(app, {
    swStatsActivate: true,
    corsDev: true
  })  
  
  await app.listen(3000)
  await fofBoostrapStats(app)
}
bootstrap()
