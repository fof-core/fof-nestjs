import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { FofCoreModule } from '@fof-nestjs/core'
import { AzureAdModule } from '@fof-nestjs/auth-azure-ad'
import { AuthApiKeyModule } from '@fof-nestjs/auth-api-key'
import { ServeStaticModule } from '@nestjs/serve-static'
import { join } from 'path'
import { CompaniesModule } from './companies/companies.module'
import { eP } from './permissions.enum'
import { companiesEntities } from './companies/entities'

const publicPath =  join(__dirname, '..', 'public')
// console.log('publicPath', publicPath)

@Module({
  imports: [
    FofCoreModule.forRoot({
      permissions: eP,
      entities: [
        ...companiesEntities
      ]
    }),
    // ServeStaticModule.forRoot({
    //   rootPath: publicPath,
    //   renderPath: '/public/*'
    //   // serveStaticOptions: {}
    // }),
    AzureAdModule,
    AuthApiKeyModule,
    CompaniesModule
  ],
  controllers: [
    AppController
  ],
  providers: [
    AppService
  ],
})
export class AppModule {
  constructor() {
    // console.log(fofEnv())
    // console.log(auth())
    
  }

  
}
